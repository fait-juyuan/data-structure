#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <stdlib.h>

//int main()
//{
//	int a = 10;
//	int b = 3;
//	int c = 4;
//	return 0;
//}


void BobbleSort(int* p, int sz)
{
	int i = 0, j = 0;
	for (i = 0; i < sz - 1; i++)
	{
		int flag = 1;
		for (j = 0; j < sz - 1 - i; j++)
		{
			if (p[j] > p[j + 1])
			{
				int tmp = p[j];
				p[j] = p[j + 1];
				p[j + 1] = tmp;
				flag = 0;
			}
		}
		if (flag == 0)
		{
			return;
		}
	}

}


int BinarySearch(int* p, int sz,int k)
{
	int left = 0;
	int right = sz - 1;
	while (left <= right)
	{
		int mid = (left + right) / 2;
		if (k < p[mid])
		{
			right = mid - 1;
		}
		else if (k > p[mid])
		{
			left = mid + 1;
		}
		else
		{
			return mid;
		}
	}
	return -1;
}



long long Fun(int n)
{
	if (n == 0)
	{
		return 1;
	}
	return Fun(n - 1)* n;
}


long long Fib(int n)
{
	if (n <= 2)
	{
		return 1;
	}
	return Fib(n - 1) + Fib(n - 2);
}


//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 ,11,34,56};
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	printf("%d ", BinarySearch(arr, sz, 7));
//
//	return 0;
//}

//int compar(const void* a, const void* b)
//{
//	return *(int*)a - *(int*)b;
//}


int compar(const void* a, const void* b)
{
	if ((*(int*)a - *(int*)b) > 0)
	{
		return 1;
	}
	else if ((*(int*)a - *(int*)b) == 0)
	{
		return 0;
	}
	else
	{
		return -1;
	}
	//return ( *(int*)a - *(int*)b );
	//这样有可能会产生溢出
}

//int main()
//{
//	int arr[] = { 9, 6, 4, 2, 3, 5, 7, 0, 1 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	//把数组变成有序
//	qsort(arr, sz, sizeof(arr[0]), compar);
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		//如果元素不等于自己对于的下标，就是缺失的数字
//		if (i != arr[i])
//		{
//			printf("缺失的数字是：%d\n ", i);
//			break;
//		}
//	}
//	return 0;
//}

int Find(int* arr, int sz)
{
	//开辟一块空间
	int* p = (int*)malloc(sz * sizeof(int));
	if (p == NULL)
	{
		perror("erron");
		exit(-1);
	}
	int i = 0;
	//把数组中元素放到等于开辟的空间下标位置
	for (i = 0; i < sz; i++)
	{
		p[arr[i]] = arr[i];
	}

	for (i = 0; i <= sz; i++)
	{
		//验证元素是否等于自己对于的下标
		if (p[i] != i)
		{
			break;
		}
	}
	return i;
}


int main()
{
	int arr1[] = { 9, 6, 4, 2, 3, 5, 7, 0, 1 };
	int arr2[] = { 3,0,1,5,4,6 };
	int arr3[] = { 5,3,4,0,2 };
	int sz1 = sizeof(arr1) / sizeof(arr1[0]);
	int sz2 = sizeof(arr2) / sizeof(arr2[0]);
	int sz3 = sizeof(arr3) / sizeof(arr3[0]);

	printf("数组arr1缺失的数字为：%d\n", Find(arr1, sz1));
	printf("数组arr2缺失的数字为：%d\n", Find(arr2, sz2));
	printf("数组arr3缺失的数字为：%d\n", Find(arr3, sz3));
	return 0;
}