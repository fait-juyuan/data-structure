#define _CRT_SECURE_NO_WARNINGS 1
#include "SeqList.h"

//对变量sl进行初始化函数
void SeqListInit(SL* ps)
{
	ps->a = NULL;
	ps->size = 0;
	ps->capacity = 0;
}

void SeqListPrint(SL* ps)
{
	int i = 0;
	for (i = 0; i < ps->size; i++)
	{
		printf("%d ", ps->a[i]);
	}
	printf("\n");
}

DataType* BuyCapacity(SL* ps)
{
	//第一次插入数据我们容量要给2，以后空间满了就用二倍来进行扩容处理
	ps->capacity = ps->capacity > 0 ? ps->capacity * 2 : 2;

	DataType* tmp = (DataType*)realloc(ps->a, sizeof(DataType) * ps->capacity);
	if (tmp == NULL)
	{
		perror("erron");
		exit(-1);
	}
	return tmp;

}

void SeqListPushBack(SL* ps, DataType x)
{
	if (ps->capacity == ps->size)  //检测空间是否满了
	{
		DataType* tmp = BuyCapacity(ps); //增容函数
		ps->a = tmp;
	}
	ps->a[ps->size] = x;   //把数据放到尾部
	ps->size++;     //下标要往后挪动一位
	//SeqListInsert(ps, x, ps->size);
}

void SeqListPushFront(SL* ps,DataType x)
{
	if (ps->capacity == ps->size)
	{
		DataType* tmp = BuyCapacity(ps);
		ps->a = tmp;
	}
	int i = 0;
	for (i = ps->size - 1; i >= 0; i--)
	{
		ps->a[i + 1] = ps->a[i];
	}
	ps->a[0] = x; //把数据放到头部
	ps->size++;  //下标要往后挪动一位

	//SeqListInsert(ps, x, 0);

}

void SeqListPopBack(SL* ps)
{
	assert(ps->size > 0); //判断空间是否有数据
	ps->size--;   //下标自减一即可
	//SeqListErase(ps, ps->size - 1);
}

void SeqListPopFront(SL* ps)
{
	//assert(ps->size > 0);
	//int i = 0;
	//for (i = 1; i < ps->size; i++)
	//{
	//	ps->a[i - 1] = ps->a[i];
	//}
	//ps->size--;

	SeqListErase(ps, 0);
}

int  SeqListFind(SL* ps, DataType x)
{
	int i = 0;
	for (i = 0; i < ps->size; i++)
	{
		if (ps->a[i] == x)
		{
			//找到数据就直接返回下标
			return i;
		}
	}
	//找不到就返回-1
	return -1;
}

void SeqListInsert(SL* ps, DataType x,int pos)
{
	//对插入的位置进行判断
	assert(pos >= 0 && pos <= ps->size);
	if (ps->capacity == ps->size)
	{
		DataType* tmp = BuyCapacity(ps);
		ps->a = tmp;
	}

	int i = 0;
	for (i = ps->size - 1; i >= pos; i--)
	{
		ps->a[i + 1] = ps->a[i];
	}
	ps->a[pos] = x;  
	ps->size++;
}

void SeqListErase(SL* ps, int pos)
{
	//要判断空间内是否有数据
	assert(ps->size != 0); 
	//要对删除的位置进行判断
	assert(pos >= 0 && pos < ps->size);

	//挪动数据
	int i = 0;
	for (i = pos + 1; i < ps->size; i++)
	{
		ps->a[i - 1] = ps->a[i];
	}
	ps->size--;
}


// 释放内存函数
void SeqListDestroy(SL* ps)
{
	free(ps->a);
	ps->a = NULL;
	ps->capacity = 0;
	ps->size = 0;
}