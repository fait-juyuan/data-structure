#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

typedef int DataType;           //要操作数据的类型，这里是int型
struct SeqList
{
	DataType* a;                    //数据的指针
	int size;                     //下标
	int capacity;                //记录开辟空间的最大下标处
};
typedef struct SeqList SL;

//当size 和 capacity相等时要进行扩容处理

//初始化函数
void SeqListInit(SL* ps);
//尾插函数
void SeqListPushBack(SL* ps,DataType x);
//头删函数
void SeqListPushFront(SL* ps,DataType x);
//尾删函数
void SeqListPopBack(SL* ps);
//头删函数
void SeqListPopFront(SL* ps);
//显示函数
void SeqListPrint(SL* ps);
//寻找函数
int SeqListFind(SL* ps, DataType x);
//插入函数
void SeqListInsert(SL* ps,DataType x ,int pos);
//删除函数
void SeqListErase(SL* ps, int pos);
//释放内存函数
void SeqListDestroy(SL* ps);