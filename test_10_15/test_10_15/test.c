#define _CRT_SECURE_NO_WARNINGS 1

int removeElement(int* nums, int numsSize, int val) {
    int start = 0;
    int end = numsSize;
    if (numsSize == 0)
    {
        return 0;
    }

    while (start < end)
    {
        if (nums[start] == val)
        {
            int tmp = nums[start];
            nums[start] = nums[end - 1];
            nums[end - 1] = tmp;
            end--;
        }
        else
        {
            start++;
        }

        //找到和val相等的元素
        //while(nums[start]!=val && start < end)
        //{
        //   start++;
        // }
        //找到和val不相等的元素
        //while(nums[end-1]==val && start < end)
        //{
        //    end--;
        //}
        //交换位置


    }
    return start;
}

int removeDuplicates(int* nums, int numsSize) {
    int i = 0;
    int j = 1;
    //给两个指针，
    if (numsSize == 0)
    {
        return 0;
    }
    while (j < numsSize)
    {
        //不相等两个指针就往后面加1
        if (nums[j] != nums[i])
        {
            i++;
            j++;
        }
        //相等j就往后面找到和i不相等的元素
        else
        {
            j++;
            while (j < numsSize)
            {
                if (nums[i] == nums[j])
                {
                    j++;
                }
                //找到和i不相等的元素就赋值给i下标的下一位内容
                //就退出，i和就继续比较
                else
                {
                    i++;
                    nums[i] = nums[j];
                    break;
                }
            }
        }
    }
    return i + 1;

}