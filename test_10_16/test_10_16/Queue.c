#define _CRT_SECURE_NO_WARNINGS 1
#include "Queue.h"

void QueueInit(Queue* pq)
{
	assert(pq);
	pq->phead = NULL;
	pq->tail = NULL;
}

void QueuePush(Queue* pq, DataType x)
{
	Node* tmp = (Node*)malloc(sizeof(Node));
	if (tmp == NULL)
	{
		perror("malloc ");
		exit(-1);
	}
	tmp->data = x;
	tmp->next = NULL;
	if (pq->tail == NULL)
	{
		pq->phead = tmp;
		pq->tail = tmp;
	}
	else
	{
		pq->tail->next = tmp;
		pq->tail = tmp;
	}
}

void QueuePop(Queue* pq)
{
	assert(pq);
	assert(pq->phead);
	// 一个节点
	if (pq->phead->next == NULL)
	{
		free(pq->phead);
		pq->phead = NULL;
		pq->tail = NULL;
	}
	//多个节点
	else
	{
		Node* tmp = pq->phead->next;
		free(pq->phead);
		pq->phead = tmp;
	}
}

int QueueSize(Queue* pq)
{
	assert(pq);
	assert(pq->phead);
	int count = 0;
	Node* cur = pq->phead;
	while (cur != NULL)
	{
		cur = cur->next;
		count++;
	}
	return count;
}

bool QueueEmpty(Queue* pq)
{
	assert(pq);
	return pq->phead == NULL;
}

DataType QueueFront(Queue* pq)
{
	assert(pq);
	assert(pq->phead);
	return pq->phead->data;
}

DataType QueueBack(Queue* pq)
{
	assert(pq);
	assert(pq->phead);
	return pq->tail->data;
}

void QueueDestroy(Queue* pq)
{
	assert(pq);
	Node* tmp = pq->phead;
	while (tmp != NULL)
	{
		Node* next = tmp->next;
		free(tmp);
		tmp = next;
	}
	pq->phead = pq->tail = NULL;
}