#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>

//ǰ������
struct BinaryTreeNode;

typedef struct BinaryTreeNode* DataType;

typedef struct Node
{
	struct Node* next;
	DataType data;
}Node;

typedef struct Queue
{
	Node* phead;
	Node* tail;
}Queue;


void QueueInit(Queue* pq);
void QueueDestroy(Queue* pq);


void QueuePush(Queue* pq,DataType x);
void QueuePop(Queue* pq);

DataType QueueBack(Queue* pq);
DataType QueueFront(Queue* pq);
int QueueSize(Queue* pq);
bool QueueEmpty(Queue* pq);
