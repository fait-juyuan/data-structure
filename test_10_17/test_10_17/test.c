#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>


void InsertSort1(int* p, int sz)
{
	int i = 0;
	
	for (i = 0; i < sz - 1; i++)
	{
		int end = i;
		int tmp = p[end + 1];
		while (end >= 0)
		{
			if (p[end] > tmp)
			{
				p[end + 1] = p[end];
				end--;
			}
			else
			{
				break;
			}
		}
		p[end + 1] = tmp;
	}
}

void InsertSort2(int* p, int sz)
{
	int i = 0;
	int end = 0;
	for (i = 0; i < sz - 1; i++)
	{
		int tmp = p[i + 1];
		for (end = i; end >= 0; end--)
		{
			if (p[end] > tmp)
			{
				p[end + 1] = p[end];
			}
			else
			{
				break;
			}
		}
		p[end + 1] = tmp;
	}
}


void ShellSort1(int* p, int sz)
{
	int gap = sz;
	while (gap > 1)
	{
		gap = gap / 2;
		int i = 0;
		int end = 0;
		for (i = 0; i < sz - gap; i++)
		{
			int tmp = p[i + gap];
			for (end = i; end >= 0; end -= gap)
			{
				if (p[end] > tmp)
				{
					p[end + gap] = p[end];
				}
				else
				{
					break;
				}
			}
			p[end + gap] = tmp;
		}
	}
}


void ShellSort2(int* p, int sz)
{
	int gap = sz;
	while (gap > 1)
	{
		gap /= 2;
		int i = 0;
		for (i = 0; i < sz - gap; i++)
		{
			int end = i;
			int tmp = p[end + gap];
			while (end >= 0)
			{
				if (p[end] > tmp)
				{
					p[end + gap] = p[end];
					end = end - gap;
				}
				else
				{
					break;
				}
			}
			p[end + gap] = tmp;
		}
	}
}

void Print(int* p, int sz)
{
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		printf("%d ", p[i]);
	}
	printf("\n ");
}

int main()
{
	int arr[] = { 5,8,9,7,11,6,4,3,2,1 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	//InsertSort2(arr, sz);
	ShellSort2(arr, sz);
	Print(arr, sz);
	return 0;
}