#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>

void Print(int* p, int sz)
{
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		printf("%d ", p[i]);
	}
	printf("\n");
}

void Swap(int* a, int* b)
{
	int tmp = *a;
	*a = *b;
	*b = tmp;
}

void AdjustDown(int* p, int n,int root)
{
	int parent = root;
	int child = parent * 2 + 1;  //默认是leftchild
	while (child <n)
	{
		if (child+1 < n  &&  p[child + 1] < p[child])
		{
			child = child + 1;
		}
		if (p[child] < p[parent])
		{
			int tmp = p[child];
			p[child] = p[parent];
			p[parent] = tmp;
			parent = child;
			child = parent * 2 + 1;
		}
		else
		{
			break;
		}
	}
}

void HeapSort(int* p, int sz)
{
	int i = 0;
	//第一步建堆
	for (i = (sz - 1 - 1) / 2; i >= 0; i--)
	{
		AdjustDown(p, sz, i);
	}
	Print(p, sz);
	int end = sz - 1;
	while (end > 0)
	{
		int tmp = p[0];
		p[0] = p[end];
		p[end] = tmp;
		AdjustDown(p, end, 0);
		end--;
	}
}

void SelectSort(int* p, int sz)
{
	int start = 0;
	int end = sz - 1;
	while (start < end)
	{
		int mini = start, maxi = start;
		int i = 0;
		for (int i = start; i <= end ; i++)
		{
			if ( p[i] < p[mini])
			{
				mini = i;
			}
			if (p[i] > p[maxi])
			{
				maxi = i;
			}
		}
		Swap(&p[start], &p[mini]);
		if (start == maxi)
		{
			start = mini;
		}
		Swap(&p[end], &p[maxi]);
		start++;
		end--;

	}
}


int main()
{
	int arr[] = { 2,3,6,8,4,9,0,1,7,5 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	//堆排序
	//第一步建堆
    //第二步交换元素
	//HeapSort(arr,sz);
	//Print(arr, sz);


	//直接插入排序
	SelectSort(arr, sz);
	Print(arr, sz);

	return 0;
}