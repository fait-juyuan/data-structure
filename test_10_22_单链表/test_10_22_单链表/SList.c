#define _CRT_SECURE_NO_WARNINGS 1
#include "SList.h"

void SListPrint(SLNode* phead)
{
	SLNode* cur = phead;
	while (cur != NULL)
	{
		printf("%d->", cur->data);
		cur = cur->next;
	}
	printf("NULL\n");
}

//新开一个结点
SLNode* BuyNewNode(SLNDataType x)
{
	SLNode* tmp = (SLNode*)malloc(sizeof(SLNode));
	if (tmp == NULL)
	{
		perror("erron ");
		exit(-1);
	}
	tmp->data = x;
	tmp->next = NULL;
	//把新开结点的地址返回去
	return tmp;
}

void SListPushBack(SLNode** pphead, SLNDataType x)
{
	assert(pphead);
	SLNode* tmp = BuyNewNode(x);  //新开一个结点
	//链表为空的情况
	if (*pphead == NULL)
	{
		*pphead = tmp;
	}
	//链表为多个节点的情况，要找到最后一个结点
	else
	{
		SLNode* cur = *pphead;
		while (cur->next != NULL)
		{
			cur = cur->next;
		}
		//把新结点的地址给最后一个结点的指针域
		cur->next = tmp;
	}
}

void SListPushFront(SLNode** pphead, SLNDataType x)
{
	assert(pphead);
	SLNode* tmp = BuyNewNode(x);
	tmp->next = *pphead;
	*pphead = tmp;
}

void SListPopBack(SLNode** pphead)
{
	assert(pphead);
	//空链表就不能再删除了
	assert(*pphead);

	//链表为一个结点的情况
	if ((*pphead)->next == NULL)
	{
		free(*pphead);
		*pphead = NULL;
	}
	//链表为多个结点的情况
	else
	{
		SLNode* cur = *pphead;
		SLNode* prev = NULL;
		while (cur->next != NULL)
		{
			prev = cur;
			cur = cur->next;
		}
		free(cur); //释放结点
		prev->next = NULL;  //把倒数第二个结点指针域置为空
	}

}

void SListPopFront(SLNode** pphead)
{
	assert(pphead);
	//空链表就不能再删除了
	assert(*pphead);


	SLNode* next = (*pphead)->next;
	free(*pphead);
	*pphead = next;
}


SLNode* SListFind(SLNode* phead,SLNDataType x)
{
	SLNode* cur = phead;
	while (cur != NULL)
	{
		if (cur->data == x)
		{
			//找到了就返回结点的地址
			return cur;
		}
		cur = cur->next;
	}
	//找不到就返回空指针
	return NULL;
}


void SListInsertPrev(SLNode** pphead, SLNode* pos, SLNDataType x)
{
	assert(pphead);
	assert(pos);
	SLNode* tmp = BuyNewNode(x);
	//如果要插入的位置在头部前面就头插
	if (pos == *pphead)
	{
		tmp->next = *pphead;
		*pphead = tmp;
	}
	//在其它位置就找前一个结点
	else
	{
		SLNode* cur = *pphead;
		while (cur->next != pos)
		{
			cur = cur->next;
		}
		cur->next = tmp;
		tmp->next = pos;
	}
}


void SListInsertAfter(SLNode* pos, SLNDataType x)
{
	assert(pos);
	SLNode* tmp = BuyNewNode(x);
	tmp->next = pos->next;
	pos->next = tmp;
}

void SListErase(SLNode** pphead, SLNode* pos)
{
	assert(pphead);
	assert(pos);
	//要删除的是第一个结点
	if (*pphead == pos)
	{
		*pphead = pos->next;
		free(pos);
	}
	//删除的是其它结点，要找到前一个结点
	else
	{
		SLNode* cur = *pphead;
		while (cur->next != pos)
		{
			cur = cur->next;
		}
		cur->next = pos->next;
		free(pos);
	}

}

void SListEraseAfter(SLNode* pos)
{
	assert(pos);
	//不能传最后一个结点的地址过来
	assert(pos->next != NULL);
	pos->next = pos->next->next;
	free(pos->next);
}









void SListDestroy(SLNode** pphead)
{
	SLNode* cur = *pphead;
	while (cur != NULL)
	{
		SLNode* next = cur->next;
		free(cur);
		cur = next;
	}
	*pphead = NULL;
}