#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

typedef int SLNDataType;

struct SListNode
{
	SLNDataType data;        //数据域
	struct SListNode* next;  //指针域
};

typedef struct SListNode SLNode;

void SListPrint(SLNode* phead);
void SListPushBack(SLNode** pphead, SLNDataType x);
void SListPushFront(SLNode** pphead, SLNDataType x);
void SListPopBack(SLNode** pphead);
void SListPopFront(SLNode** pphead);


SLNode* SListFind(SLNode* phead, SLNDataType x);
void SListInsertPrev(SLNode** pphead, SLNode* pos,SLNDataType x);
void SListInsertAfter(SLNode* pos, SLNDataType x);

//删除的是自己
void SListErase(SLNode** pphead, SLNode* pos);
void SListEraseAfter(SLNode* pos);


void SListDestroy(SLNode** phead);