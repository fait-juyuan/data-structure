#define _CRT_SECURE_NO_WARNINGS 1
#include "SList.h"


void TestPush()
{
	SLNode* plist = NULL;
	
	printf("尾插4个数据：\n\n");
	SListPushBack(&plist, 1);
	SListPushBack(&plist, 2);
	SListPushBack(&plist, 3);
	SListPushBack(&plist, 4);
	SListPrint(plist);

	printf("头插两个数据：\n");
	SListPushFront(&plist, 8);
	SListPushFront(&plist, 9);
	SListPushFront(&plist, 10);
	SListPrint(plist);
	SListDestroy(&plist);

}

void TestPop()
{
	SLNode* plist = NULL;

	SListPushBack(&plist, 1);
	SListPushBack(&plist, 2);
	SListPushBack(&plist, 3);
	SListPushBack(&plist, 4);
	SListPrint(plist);

	printf("尾删两个数据：\n");
	SListPopBack(&plist);
	SListPopBack(&plist);
	SListPrint(plist);

	printf("头删两个数据：\n");
	SListPopFront(&plist);
	SListPopFront(&plist);
	SListPrint(plist);
	SListDestroy(&plist);
}

void TestInsert()
{
	SLNode* plist = NULL;

	SListPushBack(&plist, 1);
	SListPushBack(&plist, 2);
	SListPushBack(&plist, 3);
	SListPushBack(&plist, 4);
	SListPushBack(&plist, 5);
	SListPrint(plist);

	SLNode* pos = SListFind(plist, 1);
	if (pos != NULL)
	{
		printf("在1的前面插入99:\n");
		SListInsertPrev(&plist, pos, 99);
		SListPrint(plist);
	}
	pos = SListFind(plist, 4);
	if (pos != NULL)
	{
		printf("在4的前面插入88:\n");
		SListInsertPrev(&plist, pos, 88);
		SListPrint(plist);
	}

	pos = SListFind(plist, 5);
	if (pos != NULL)
	{
		printf("在5的后面插入99:\n");
		SListInsertAfter(pos, 55);
		SListPrint(plist);

	}
	SListDestroy(&plist);
}

void TestErase()
{
	SLNode* plist = NULL;
	SListPushBack(&plist, 1);
	SListPushBack(&plist, 2);
	SListPushBack(&plist, 3);
	SListPushBack(&plist, 4);
	SListPushBack(&plist, 5);
	SListPrint(plist);

	SLNode* pos = SListFind(plist, 2);
	if (pos != NULL)
	{
		printf("把2删除掉:\n");
		SListErase(&plist, pos);
		SListPrint(plist);

	}

     pos = SListFind(plist, 4);
	if (pos != NULL)
	{
		printf("把4删除掉:\n");
		SListErase(&plist, pos);
		SListPrint(plist);
	}

	pos = SListFind(plist, 3);
	if (pos != NULL)
	{
		printf("删除掉2后面的下一个数据:\n");
		SListEraseAfter(pos);
		SListPrint(plist);
	}
	SListDestroy(&plist);

}

int main()
{
	TestPush();
	TestPop();
	TestInsert();
	TestErase();
	return 0;
}