#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>

void Print(int* p, int sz)
{
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		printf("%d ", p[i]);
	}
}

void QuickSort(int* p,int left,int right )
{
	if (left >= right)
	{
		return;
	}
	int start = left;
	int end = right;
	int key = p[start];
	int pivot = start;
	while (start < end)
	{
		while (start<end && p[end]>=key)
		{
			end--;
		}
		p[pivot] = p[end];
		pivot = end;
		while (start < end && p[start] <= key)
		{
			start++;
		}
		p[pivot] = p[start];
		pivot = start;
	}
	//pivot = start;
	p[pivot] = key;
	QuickSort(p, left, pivot - 1);
	QuickSort(p, pivot + 1, right);

}


int main()
{
	int arr[] = { 6,3,5,9,0,1,2,4,8,7 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	QuickSort(arr, 0, sz - 1);
	Print(arr, sz);
	return 0;
}