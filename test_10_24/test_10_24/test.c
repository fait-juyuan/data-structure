#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>

typedef struct ListNode {
    int val;
    struct ListNode* next;
   // ListNode(int x) : val(x), next(NULL) {}
}ListNode;

ListNode* partition(ListNode* pHead, int x) {
    struct ListNode* lessHead = (struct ListNode*)malloc(sizeof(struct ListNode));
    struct ListNode* lessTail = lessHead;
    struct ListNode* greatHead = (struct ListNode*)malloc(sizeof(struct ListNode));
    struct ListNode* greatTail = greatHead;
    struct ListNode* cur = pHead;
    while (cur != NULL)
    {
        if (cur->val < x)
        {
            lessTail->next = cur;
            lessTail = cur;
            cur = cur->next;
        }
        else
        {
            greatTail->next = cur;
            greatTail = cur;
            cur = cur->next;
        }
    }
    greatTail->next = NULL;
    lessTail->next = greatHead->next;
    struct ListNode* tmp = lessHead->next;
    //free(greatHead);
    //free(lessHead);
    return tmp;

}

//头插法
//写法1：
struct ListNode* reverseList(struct ListNode* head) {
    if (head == NULL)
    {
        return head;
    }
    struct ListNode* prev = NULL;
    struct ListNode* cur = head;

    while (cur != NULL)
    {

        struct ListNode* next = cur->next;
        cur->next = prev;

        //迭代过程
        prev = cur;
        cur = next;


    }
    return prev;

}



int main()
{
    struct ListNode* a = (struct ListNode*)malloc(sizeof(struct ListNode));
    struct ListNode* b = (struct ListNode*)malloc(sizeof(struct ListNode));
    struct ListNode* c = (struct ListNode*)malloc(sizeof(struct ListNode));
    struct ListNode* d = (struct ListNode*)malloc(sizeof(struct ListNode));
    struct ListNode* e = (struct ListNode*)malloc(sizeof(struct ListNode));
    struct ListNode* f = (struct ListNode*)malloc(sizeof(struct ListNode));
    a->val = 5;
    b->val = 8;
    c->val = 2;
    d->val = 3;
    e->val = 5;
    f->val = 2;
 

    a->next = b;
    b->next = c;
    c->next = d;
    d->next = e;
    e->next = f;
    f->next = NULL;
    partition(a, 4);

    return 0;
}