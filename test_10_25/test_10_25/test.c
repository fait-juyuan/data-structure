#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <stdbool.h>


  struct ListNode {
     int val;
     struct ListNode* next;
 };
 
bool hasCycle(struct ListNode* head) {
    struct ListNode* slow = head;
    struct ListNode* fast = head;
    //快指针走两步，慢指针走一步，它们一定会在环中相遇
    //没有环，快指针会走到链表末尾
    while (fast && fast->next)
    {
        fast = fast->next->next;
        slow = slow->next;
        if (fast == slow)
        {
            return true;
        }
    }
    return false;



}



struct ListNode* detectCycle(struct ListNode* head) {
    //先求出它们的环中的相遇点
    //然后慢指针从起点开始一步一步地跑
    //快指针从相遇点同样速度跑，两个指针相等时就是入口点
    struct ListNode* slow = head;
    struct ListNode* fast = head;
    struct ListNode* meet = NULL;
    while (fast && fast->next)
    {
        slow = slow->next;
        fast = fast->next->next;
        if (slow == fast)
        {
            break;
        }
    }
    //表示无环
    if (fast == NULL || fast->next == NULL)
    {
        return NULL;
    }
    else
    {
        //有环，慢指针从起点开始跑，快指针从相遇点开始跑
        //它们的速度相等
        slow = head;
        while (slow != fast)
        {
            slow = slow->next;
            fast = fast->next;
        }
        return fast;
    }

}