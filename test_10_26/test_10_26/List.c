#define _CRT_SECURE_NO_WARNINGS 1

#include "List.h"

void ListInit(LNode** pphead)
{
	LNode* tmp = (LNode*)malloc(sizeof(LNode));
	if (tmp == NULL)
	{
		perror("erron ");
		exit(-1);
	}

	tmp->next = tmp;
	tmp->prev = tmp;
	tmp->data = 0;
	*pphead = tmp;
}

void ListPrint(LNode* phead)
{
	assert(phead);
	//从哨兵位的下一个结点开始遍历
	LNode* cur = phead->next;

	//等于哨兵位就是回到起始点，停止遍历
	while (cur != phead)
	{
		printf("%d->", cur->data);
		cur = cur->next;
	}
	printf("NULL\n");
}

LNode* BuyNewNode(DataTypedef x)
{
	LNode* newNode = (LNode*)malloc(sizeof(LNode));
	if (newNode == NULL)
	{
		perror("erron ");
		exit(-1);
	}
	newNode->data = x;
	return newNode;
}

void ListPushBack(LNode* phead,DataTypedef x)
{
	assert(phead);
	//LNode* newNode = BuyNewNode(x); //开辟一个结点

	////找到最后一个结点
	//LNode* tail = phead->prev;

	//// tail   newNode  phead;
	////在最后一个结点后面链接一个新的结点
	//tail->next = newNode;
	//newNode->prev = tail;
	//phead->prev = newNode;
	//newNode->next = phead;


	//在哨兵位结点前面插入就是尾插
	ListInsert(phead, x);
}

void ListPushFront(LNode* phead, DataTypedef x)
{
	assert(phead);
	//LNode* newNode = BuyNewNode(x);
	////先找到第一个结点
	//LNode* next = phead->next;

	////phead   newNode   next
	////在哨兵位结点和第一个结点之间插入即可
	//phead->next = newNode;
	//newNode->prev = phead;
	//newNode->next = next;
	//next->prev = newNode;


	//在哨兵位结点的下一个结点前面插入就是头插
	//直接调用插入函数
	ListInsert(phead->next, x);
}


void ListPopBack(LNode* phead)
{
	assert(phead);
	//要保证链表有结点可以删除，不能删除哨兵位
	assert(phead->next != phead);
	//LNode* tail = phead->prev;
	////要找到尾结点的前一个
	//LNode* tailPrev = tail->prev;

	////tailPrev  phead
	////把尾结点的前一个和哨兵位连接起来即可
	//tailPrev->next = phead;
	//phead->prev = tailPrev;
	//
	////最后要把尾结点释放掉
	//free(tail);
	

	//删除哨兵位结点的前面一位就是尾删
	ListErase(phead->prev);
}

void ListPopFront(LNode* phead)
{
	assert(phead);
	//要保证链表有结点可以删除，不能删除哨兵位
	assert(phead->next != phead);

	////找到第二个结点
	//LNode* start = phead->next->next;
	////释放掉头结点
	//free(phead->next);


	////phead   start
	////把哨兵位和第二个结点连接起来即可
	//phead->next = start;
	//start->prev = phead;


	//删除哨兵位的下一个结点就是头删
	ListErase(phead->next);
}


LNode* ListFind(LNode* phead, DataTypedef x)
{
	assert(phead);
	//从哨兵位的下一个结点开始找
	LNode* cur = phead->next;
	while (cur != phead)
	{
		if (cur->data == x)
		{
			return cur;
		}
		cur = cur->next;
	}
	//找不到就返回空指针
	return NULL;
}

//在结点前面插入
void ListInsert(LNode* pos, DataTypedef x)
{
	assert(pos);
	//找到pos位置前面一个结点
	LNode* posPrev = pos->prev;
	LNode* newNode = BuyNewNode(x);


	//posPrev  newNode  pos
	//在它们之间插入即可
	posPrev->next = newNode;
	newNode->prev = posPrev;
	newNode->next = pos;
	pos->prev = newNode;

}

void ListErase(LNode* pos)
{
	assert(pos);
	//找到pos位置前一个和后一个结点
	LNode* posPrev = pos->prev;
	LNode* posNext = pos->next;

	// posPrev   posNext
	//把它们连起来即可
	posPrev->next = posNext;
	posNext->prev = posPrev;
}

void ListDestroy(LNode** pphead)
{
	assert(pphead);
	//从哨兵位的下一个结点开始释放
	LNode* cur = (*pphead)->next;
	while (cur != *pphead)
	{
		LNode* next = cur->next;
		free(cur);
		cur = next;
	}
	//最后把哨兵位给释放掉
	free(*pphead);
	*pphead = NULL;
}