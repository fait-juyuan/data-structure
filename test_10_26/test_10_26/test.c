#define _CRT_SECURE_NO_WARNINGS 1
#include "List.h"


//void ListPopBack(LNode* phead)
//{
//	assert(phead->next != phead);
//	LNode* tail = phead->prev;
//	LNode* tailprev = tail->prev;
//	// tail
//	free(tail);
//	tailprev->next = phead;
//	phead->prev = tail;
//}


void testPush()
{
	LNode* plist = NULL;
	ListInit(&plist);

	ListPushBack(plist, 1);
	ListPushBack(plist, 2);
	ListPushBack(plist, 3);
	ListPushBack(plist, 4);
	ListPushBack(plist, 5);
	ListPrint(plist);


	ListPushFront(plist, 6);
	ListPushFront(plist, 7);
	ListPushFront(plist, 8);
	ListPushFront(plist, 9);
	ListPrint(plist);

	ListPopFront(plist);
	ListPopFront(plist);
	ListPopFront(plist);
	ListPopFront(plist);

	ListPrint(plist);
	ListDestroy(&plist);
}

void testPop()
{
	LNode* plist = NULL;
	ListInit(&plist);

	ListPushBack(plist, 1);
	ListPushBack(plist, 2);
	ListPushBack(plist, 3);
	ListPushBack(plist, 4);
	ListPushBack(plist, 5);
	ListPrint(plist);


	ListPopBack(plist);
	ListPopBack(plist);
	ListPrint(plist);

	ListPopFront(plist);
	ListPrint(plist);
}


void testInsert()
{
	LNode* plist = NULL;
	ListInit(&plist);

	ListPushBack(plist, 1);
	ListPushBack(plist, 2);
	ListPushBack(plist, 3);
	ListPushBack(plist, 4);
	ListPushBack(plist, 5);
	ListPrint(plist);

	LNode* pos = ListFind(plist, 1);
	ListInsert(pos, 20);
	ListPrint(plist);

	pos = ListFind(plist, 3);
	ListErase(pos);
	ListPrint(plist);


}

int main()
{
	testPush();
	testPop();
	testInsert();
}