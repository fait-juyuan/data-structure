#pragma once
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>

typedef int DataTypedef;                  //对int类型重新起个名字叫DataType

struct ListNode
{
	DataTypedef data;       //数据域
	struct ListNode* next;  //指针域
	struct ListNode* prev;  //指针域
};


typedef struct ListNode LNode;               //对链表类型struct ListNode重新起个名字叫LNode

void ListInit(LNode** pphead);

void ListPrint(LNode* phead);
void ListPushBack(LNode* phead, DataTypedef x);
void ListPushFront(LNode* phead, DataTypedef x);

void ListPopBack(LNode* phead);
void ListPopFront(LNode* phead);
LNode* ListFind(LNode* phead, DataTypedef x);

//在结点之前插入
void ListInsert(LNode* pos, DataTypedef x);

//删除当前位置结点
void ListErase(LNode* pos);
void ListDestroy(LNode** pphead);