#define _CRT_SECURE_NO_WARNINGS 1
#include "Stack.h"

void test()
{
	ST st;
	StackInit(&st);

	printf("��1 2 3 4 5 6��ջ\n");
	StackPush(&st, 1);
	StackPush(&st, 2);
	StackPush(&st, 3);
	StackPush(&st, 4);
	StackPush(&st, 5);
	StackPush(&st, 6);

	printf("��ջ��\n");
	while (!StackEmpty(&st))
	{
		printf("%d ", StackTop(&st));
		StackPop(&st);
	}

	StackDestroy(&st);
}

int main()
{
	test();
	return 0;
}