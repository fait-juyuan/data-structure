#define _CRT_SECURE_NO_WARNINGS 1

//原地移除数组中所有的元素val
int removeElement(int* nums, int numsSize, int val) {
    int left = 0;
    int right = 0;
    while (right < numsSize)
    {
        //如果等于val，left不动，right往后走一步
        if (nums[right] == val)
        {
            right++;
        }
        //如果不等于val，把right的值赋值给left，
        //同时left和right都往后走一步
        else
        {
            nums[left] = nums[right];
            left++;
            right++;
        }
    }
    return left;
}


//删除排序数组中的重复项
int removeDuplicates(int* nums, int numsSize) {
    int i = 0;
    int j = 1;
    //给两个指针，
    if (numsSize == 0)
    {
        return 0;
    }
    while (j < numsSize)
    {
        //不相等两个指针就往后面加1
        if (nums[j] != nums[i])
        {
            i++;
            j++;
        }
        //相等j就往后面找到和i不相等的元素
        else
        {
            while (j < numsSize)
            {
                if (nums[i] == nums[j])
                {
                    j++;
                }
                //找到和i不相等的元素就赋值给i下标的下一位内容
                //就退出，i和j就继续比较
                else
                {
                    i++;
                    nums[i] = nums[j];
                    break;
                }
            }
        }
    }
    return i + 1;

}

//合并两个有序数组
void merge(int* nums1, int nums1Size, int m, int* nums2, int nums2Size, int n) {
    //类似于归并排序，要从后面开始排
    //从两个数组的最后一个元素开始比较
    //大的就先往数组1后面放
    int i = m - 1;
    int j = n - 1;
    int end = m + n - 1;
    while (i >= 0 && j >= 0)
    {
        //比较数组末端的元素大小，大的往后面放
        if (nums1[i] > nums2[j])
        {
            nums1[end] = nums1[i];
            i--;
            end--;
        }
        else
        {
            nums1[end] = nums2[j];
            j--;
            end--;
        }
    }
    //排完后还有三种情况，
    //1.两个数组元素刚刚好放完
    //2.把数组2的所以元素都放完了，还剩下数组1的一些元素
    //3.把数组1的所有元素都放完了，还剩下数组2的一些元素
    //这3种情况我们只需要处理第三种就可以了，
    //只有情况3没有把所有的元素都整合到数组1中。

    if (j >= 0)
    {
        while (j >= 0)
        {
            nums1[end] = nums2[j];
            j--;
            end--;
        }
    }

}

