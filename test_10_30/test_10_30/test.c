#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

 struct ListNode {
	int val;
	struct ListNode* next;
};

 struct ListNode* removeElements(struct ListNode* head, int val) {
     struct ListNode* cur = head;
     struct ListNode* next = NULL;
     struct ListNode* newHead = NULL;
     struct ListNode* prev = NULL;
     while (cur != NULL)
     {
         if (cur->val == val)
         {
             if (cur == head)
             {
                 newHead = cur->next;
                 free(cur);
                 cur = newHead;
             }
             else
             {
                 next = cur->next;
                 free(cur);
                 prev->next = next;
                 cur = next;
             }
         }
         else
         {
             prev = cur;
             next = cur->next;
             cur = next;
         }
     }
     return newHead;

 }


int main()
{
	struct ListNode* a = (struct ListNode*)malloc(sizeof(struct ListNode));
	struct ListNode* b = (struct ListNode*)malloc(sizeof(struct ListNode));
	struct ListNode* c = (struct ListNode*)malloc(sizeof(struct ListNode));
	struct ListNode* d = (struct ListNode*)malloc(sizeof(struct ListNode));
	struct ListNode* e = (struct ListNode*)malloc(sizeof(struct ListNode));
	a->val = 1;
	b->val = 2;
	c->val = 6;
	d->val = 5;
	e->val = 6;


	a->next = b;
	b->next = c;
	c->next = d;
	d->next = e;
	e->next = NULL;


    removeElements(a, 6);

	return 0;
}


// 移除链表元素
struct ListNode* removeElements(struct ListNode* head, int val) {
    struct ListNode* cur = head;
    struct ListNode* next = NULL;
    struct ListNode* prev = NULL;
    struct ListNode* newHead = head;
    while (cur != NULL)
    {
        //等于val有2种情况
        //1.头节点为val
        if (cur->val == val)
        {
            if (cur == head)
            {
                newHead = cur->next;
                free(cur);
                cur = newHead;
                head = newHead;
            }
            //2.其它节点为val
            else
            {
                next = cur->next;
                free(cur);
                prev->next = next;
                cur = next;
            }
        }
        //不等于val就迭代往后走
        else
        {
            prev = cur;
            next = cur->next;
            cur = next;
        }
    }
    return newHead;

}


struct ListNode* removeElements(struct ListNode* head, int val) {
    //开辟一个哨兵位结点，哨兵结点的指针域存放链表的头指针
    struct ListNode* newHead =
        (struct ListNode*)malloc(sizeof(struct ListNode));
    newHead->next = head;

    struct ListNode* prev = newHead;
    struct ListNode* cur = newHead->next;
    struct ListNode* next = NULL;
    while (cur != NULL)
    {
        //等于val就删除改结点，并把前后结点链接起来
        if (cur->val == val)
        {
            next = cur->next;
            free(cur);
            prev->next = next;
            cur = next;
        }
        //不等于val就找下一个
        else
        {
            prev = cur;
            next = cur->next;
            cur = next;
        }
    }
    //释放哨兵位结点，返回哨兵位结点的指针域即（链表的头结点）
    struct ListNode* tmp = newHead->next;
    free(newHead);
    return tmp;

}


struct ListNode* reverseList(struct ListNode* head) {
    if (head == NULL)
    {
        return head;
    }
    struct ListNode* prev = NULL;
    struct ListNode* cur = head;

    while (cur != NULL)
    {
        struct ListNode* next = cur->next;
        //让该结点指针域存放上一个结点的地址
        cur->next = prev;

        //迭代过程
        prev = cur;
        cur = next;
    }
    return prev;

}


struct ListNode* getKthFromEnd(struct ListNode* head, int k) {
    struct ListNode* slow = head;
    struct ListNode* fast = head;
    //先让快指针走k步，这样快指针和慢指针的距离就是k步
    while (k--)
    {
        //防止k过大，造成对空指针进行解引用
        if (fast == NULL)
        {
            return NULL;
        }
        fast = fast->next;
    }

    //快指针走一步，慢指针也走一步
    //当快指针走到空指针时，慢指针还是距离快指针k步
    //这样慢指针就是倒数第k个结点
    while (fast != NULL)
    {
        slow = slow->next;
        fast = fast->next;
    }
    return slow;


}



struct ListNode* middleNode(struct ListNode* head) {
    struct ListNode* slow = head;
    struct ListNode* fast = head;
    //有两种情况快指针要停下
    while (fast && fast->next)
    {
        slow = slow->next;
        fast = fast->next->next;
    }
    return slow;

}



struct ListNode* MidListNode(struct ListNode* head)
{
    struct ListNode* slow = head;
    struct ListNode* fast = head;
    while (fast && fast->next)
    {
        slow = slow->next;
        fast = fast->next->next;
    }
    return slow;
}

struct ListNode* reverseList(struct ListNode* head)
{
    struct ListNode* cur = head;
    struct ListNode* prev = NULL;
    while (cur != NULL)
    {
        struct ListNode* next = cur->next;
        cur->next = prev;

        prev = cur;
        cur = next;
    }
    return prev;
}


bool isPalindrome(struct ListNode* head) {
    //先求出中间结点
    struct ListNode* mid = MidListNode(head);
    //然后以中间结点为开始，反转后面的结点
    struct ListNode* headB = reverseList(mid);
    struct ListNode* headA = head;

    //接着开始对比两个链表是否相同
    //不管链表结点是否是奇数还是偶数
    //当任意一个链表走到空时，两个链表内容就全部相等，即是回文链表
    while (headA && headB)
    {
        if (headA->val != headB->val)
        {
            return false;
        }
        headA = headA->next;
        headB = headB->next;
    }
    return true;

}