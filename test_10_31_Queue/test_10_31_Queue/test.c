#define _CRT_SECURE_NO_WARNINGS 1
#include "Queue.h"

void testQueue()
{
	Queue q;



	QueueInit(&q);

	QueuePush(&q, 1);
	QueuePush(&q, 2);
	QueuePush(&q, 3);
	
	QueuePop(&q);
	QueuePop(&q);
	QueuePop(&q);



	QueuePush(&q, 4);
	QueuePush(&q, 5);
	QueuePush(&q, 6);


	while (!QueueEmpty(&q))
	{
		printf("%d ", QueueFront(&q));
		QueuePop(&q);
	}



}



int main()
{
	testQueue();
	return 0;
}