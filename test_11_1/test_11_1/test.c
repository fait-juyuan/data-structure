#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <assert.h>



typedef int STDataType;

struct Stack
{
	STDataType* a;
	int top;
	int capacity;
};
typedef struct Stack ST;

void StackInit(ST* ps);
void StackPush(ST* ps, STDataType x);
void StackPop(ST* ps);
STDataType StackTop(ST* ps);
int StackSize(ST* ps);
bool StackEmpty(ST* ps);
void StackDestroy(ST* ps);

void StackInit(ST* ps)
{
	ps->a = NULL;
	ps->capacity = 0;
	ps->top = 0;
}

void StackPush(ST* ps, STDataType x)
{
	assert(ps);
	//内存满了要扩容
	if (ps->capacity == ps->top)
	{
		ps->capacity = ps->capacity > 0 ? ps->capacity * 2 : 2;
		STDataType* tmp = (STDataType*)realloc(ps->a, sizeof(STDataType) * ps->capacity);
		if (tmp == NULL)
		{
			perror("erron ");
			exit(-1);
		}
		ps->a = tmp;
	}
	ps->a[ps->top] = x;
	ps->top++;

}

void StackPop(ST* ps)
{
	assert(ps);
	//栈为空就不能再出栈了
	assert(ps->top >= 1);

	ps->top--;
}

STDataType StackTop(ST* ps)
{
	assert(ps);
	assert(ps->top >= 1);
	return ps->a[ps->top - 1];

}

int StackSize(ST* ps)
{
	assert(ps);
	assert(ps->top >= 1);
	return ps->top - 1;
}


bool StackEmpty(ST* ps)
{
	return ps->top == 0;
}


void StackDestroy(ST* ps)
{
	free(ps->a);
	ps->a = NULL;
	ps->top = 0;
	ps->capacity = 0;
}

//两个栈，把不为空的栈往为空的栈里面倒过来就是队列了
typedef struct {
	ST Pushst;
	ST Popst;
} MyQueue;


MyQueue* myQueueCreate() {
	MyQueue* tmp = (MyQueue*)malloc(sizeof(MyQueue));
	if (tmp == NULL)
	{
		perror("erron ");
		exit(-1);
	}
	StackInit(&tmp->Pushst);
	StackInit(&tmp->Popst);
	return tmp;

}

void myQueuePush(MyQueue* obj, int x) {

	StackPush(&obj->Pushst, x);

}

int myQueuePop(MyQueue* obj) {
	if (StackEmpty(&obj->Popst))
	{
		while (!StackEmpty(&obj->Pushst))
		{
			StackPush(&obj->Popst, StackTop(&obj->Pushst));
			StackPop(&obj->Pushst);
		}
	}
	int tmp = StackTop(&obj->Popst);
	StackPop(&obj->Popst);
	return tmp;
}

int myQueuePeek(MyQueue* obj) {
	if (StackEmpty(&obj->Popst))
	{
		while (!StackEmpty(&obj->Pushst))
		{
			StackPush(&obj->Popst, StackTop(&obj->Pushst));
			StackPop(&obj->Pushst);
		}
	}
	return StackTop(&obj->Popst);

}

bool myQueueEmpty(MyQueue* obj) {
	return StackEmpty(&obj->Pushst) && StackEmpty(&obj->Popst);
}

void myQueueFree(MyQueue* obj) {
	free(&obj->Pushst);
	free(&obj->Popst);
	free(obj);

}

//
//int main()
//{
//	MyQueue* obj = myQueueCreate();
//	myQueuePush(obj, 1);
//	myQueuePush(obj, 2);
//	int front = myQueuePeek(obj);
//	int pop = myQueuePop(obj);
//
//	printf("%d %d", front, pop);
//	return 0;
//}










//环行队列
typedef struct Node
{
	struct Node* next;
	int data;
}Node;


typedef struct {
	Node* front;
	Node* tail;
} MyCircularQueue;


MyCircularQueue* myCircularQueueCreate(int k) {
	MyCircularQueue* tmp = (MyCircularQueue*)malloc(sizeof(MyCircularQueue));

	//构造一个环链表
	Node* head = (Node*)malloc(sizeof(Node));
	Node* a = head;
	Node* tail = head;
	k = k - 1;
	while (k--)
	{
		a = tail;
		tail = (Node*)malloc(sizeof(Node));
		a->next = tail;
	}
	tail->next = head;

	tmp->front = head;
	tmp->tail = head;
	return tmp;

}

bool myCircularQueueEnQueue(MyCircularQueue* obj, int value) {
	if (obj->tail->next == obj->tail)
	{
		return false;
	}
	obj->tail->next->data = value;
	obj->tail = obj->tail->next;
	return true;

}

bool myCircularQueueDeQueue(MyCircularQueue* obj) {
	if (obj->front == obj->tail)
	{
		return false;
	}
	obj->front = obj->front->next;
	return true;

}

int myCircularQueueFront(MyCircularQueue* obj) {
	if (obj->front == obj->tail)
	{
		return -1;
	}
	return obj->front->data;

}

int myCircularQueueRear(MyCircularQueue* obj) {
	if (obj->front == obj->tail)
	{
		return -1;
	}
	return obj->tail->data;

}

bool myCircularQueueIsEmpty(MyCircularQueue* obj) {
	return obj->front == obj->tail;

}

bool myCircularQueueIsFull(MyCircularQueue* obj) {
	return obj->tail->next = obj->front;

}

void myCircularQueueFree(MyCircularQueue* obj) {
	Node* cur = obj->front;
	while (cur != obj->tail)
	{
		Node* next = cur->next;
		free(cur);
		cur = next;
	}
	free(cur);
	free(obj);

}

int main()
{
	MyCircularQueue* obj = myCircularQueueCreate(3);
	myCircularQueueEnQueue(obj, 1);
	myCircularQueueEnQueue(obj, 2);
	myCircularQueueEnQueue(obj, 3);
	myCircularQueueEnQueue(obj, 4);

	return 0;
}


//用栈实现队列

typedef int STDataType;

struct Stack
{
	STDataType* a;
	int top;
	int capacity;
};
typedef struct Stack ST;

void StackInit(ST* ps);
void StackPush(ST* ps, STDataType x);
void StackPop(ST* ps);
STDataType StackTop(ST* ps);
int StackSize(ST* ps);
bool StackEmpty(ST* ps);
void StackDestroy(ST* ps);

void StackInit(ST* ps)
{
	ps->a = NULL;
	ps->capacity = 0;
	ps->top = 0;
}

void StackPush(ST* ps, STDataType x)
{
	assert(ps);
	//内存满了要扩容
	if (ps->capacity == ps->top)
	{
		ps->capacity = ps->capacity > 0 ? ps->capacity * 2 : 2;
		STDataType* tmp = (STDataType*)realloc(ps->a, sizeof(STDataType) * ps->capacity);
		if (tmp == NULL)
		{
			perror("erron ");
			exit(-1);
		}
		ps->a = tmp;
	}
	ps->a[ps->top] = x;
	ps->top++;

}

void StackPop(ST* ps)
{
	assert(ps);
	//栈为空就不能再出栈了
	assert(ps->top >= 1);

	ps->top--;
}

STDataType StackTop(ST* ps)
{
	assert(ps);
	assert(ps->top >= 1);
	return ps->a[ps->top - 1];

}

int StackSize(ST* ps)
{
	assert(ps);
	assert(ps->top >= 1);
	return ps->top - 1;
}


bool StackEmpty(ST* ps)
{
	return ps->top == 0;
}


void StackDestroy(ST* ps)
{
	free(ps->a);
	ps->a = NULL;
	ps->top = 0;
	ps->capacity = 0;
}

//两个栈，把不为空的栈往为空的栈里面倒过来就是队列了
//设计两个栈，一个用来入数据，一个用来出数据
//入数据就入pushst栈，出数据就出popst栈
typedef struct {
	ST Pushst;
	ST Popst;
} MyQueue;


MyQueue* myQueueCreate() {
	MyQueue* tmp = (MyQueue*)malloc(sizeof(MyQueue));
	if (tmp == NULL)
	{
		perror("erron ");
		exit(-1);
	}
	StackInit(&tmp->Pushst);
	StackInit(&tmp->Popst);
	return tmp;

}

void myQueuePush(MyQueue* obj, int x) {

	StackPush(&obj->Pushst, x);

}

//要检验出数据的栈是否为空，没有数据了就从入数据的栈里面取
int myQueuePop(MyQueue* obj) {
	if (StackEmpty(&obj->Popst))
	{
		while (!StackEmpty(&obj->Pushst))
		{
			StackPush(&obj->Popst, StackTop(&obj->Pushst));
			StackPop(&obj->Pushst);
		}
	}
	int tmp = StackTop(&obj->Popst);
	StackPop(&obj->Popst);
	return tmp;
}

//取队列的头元素，检验出数据的栈是否为空，没有数据了就从入数据的栈里面取
int myQueuePeek(MyQueue* obj) {
	if (StackEmpty(&obj->Popst))
	{
		while (!StackEmpty(&obj->Pushst))
		{
			StackPush(&obj->Popst, StackTop(&obj->Pushst));
			StackPop(&obj->Pushst);
		}
	}
	return StackTop(&obj->Popst);

}

bool myQueueEmpty(MyQueue* obj) {
	return StackEmpty(&obj->Pushst) && StackEmpty(&obj->Popst);
}

void myQueueFree(MyQueue* obj) {
	StackDestroy(&obj->Pushst);
	StackDestroy(&obj->Pushst);
	free(obj);

}