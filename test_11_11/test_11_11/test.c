#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>


void Print(int* a, int n)
{
	int i = 0;
	for (i = 0; i < n; i++)
	{
		printf("%d ", a[i]);
	}
	printf("\n");
}


void Swap(int* px, int* py)
{
	int tmp = *px;
	*px = *py;
	*py = tmp;
}

void AdjustUp(int* a, int child)
{
	int parent = (child - 1) / 2;
	while (child > 0)
	{
		if (a[child] < a[parent])
		{
			Swap(&a[child], &a[parent]);
			child = parent;
			parent = (child - 1) / 2;
		}
		else
		{
			break;
		}
	}
}

void AdjustDowm(int* a, int n, int parent)
{
	int child = parent * 2 + 1;
	while (child < n)
	{
		if (child + 1 < n && a[child + 1] < a[child])
		{
			child++;
		}
		if (a[child] < a[parent])
		{
			Swap(&a[child], &a[parent]);
			parent = child;
			child = parent * 2 + 1;
		}
		else
		{
			break;
		}
	}
}

void HeapSort(int* a, int n)
{
	//int i = 0;
	//for (i = 1; i < n; i++)
	//{
	//	AdjustUp(a, i);
	//}
	//Print(a, n);

	int end = 0;
	for (end = (n - 1 - 1) / 2; end >= 0; end--)
	{
		AdjustDowm(a, n, end);
	}
	Print(a, n);

	for (end = n - 1; end > 0; end--)
	{
		Swap(&a[0], &a[end]);
		AdjustDowm(a, end, 0);
	}
	Print(a, n);


}

int main()
{
	int arr[] = { 2,3,5,7,9,1,0,4,6 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	HeapSort(arr, sz);
	return 0;
}


int BinaryTreeComplete(BTNode* root)
{
	Queue q;
	QueueInit(&q);
	Queuepush(&q, root);
	while (!QueueEmpty(&q))
	{
		BTNode* front = QueueFront(&q);
		QueuePop(&q);
		if (front == NULL)
		{
			while (!QueueEmpty(&q))
			{
				BTNode* front = QueueFront(&q);
				if (front != NULL)
				{
					return false;
				}
				QueuePop(&q);
			}
		}
		if (front != NULL)
		{
			QueuePush(&q, front->left);
			QueuePush(&q, front->right);
		}
	}
	if()
}