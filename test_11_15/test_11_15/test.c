#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>

void Swap(int* px, int* py)
{
	int tmp = *px;
	*px = *py;
	*py = tmp;
}

void Print(int* p,int n)
{
	for (int i = 0; i < n; i++)
	{
		printf("%d ", p[i]);
	}
	printf("\n");
}

void InsertSort(int* p, int n)
{
	for(int i=0;i<n-1;i++)
	{
		int end = i;
		int x = p[end + 1];
		while (end >= 0)
		{
			if (x < p[end])
			{
				p[end + 1] = p[end];
				end--;
			}
			else
			{
				break;
			}
		}
		p[end + 1] = x;
	}
}

void ShellSort(int* p, int n)
{
	int gap = n;
	while (gap>1)
	{
		gap = gap / 2;
		for (int i = 0; i < n - gap; i++)
		{
			int end = i;
			int x = p[end + gap];
			while (end >= 0)
			{
				if (x < p[end])
				{
					p[end + gap] = p[end];
					end = end - gap;
				}
				else
				{
					break;
				}
			}
			p[end + gap] = x;
		}
	}

}

void SelectSort(int* p, int n)
{
	int begin = 0;
	int end = n-1;

	while (begin < end)
	{
		//分别记录最小值和最大值的下标
		int mini = begin;
		int maxi = begin;
		for (int i = begin; i <= end; i++)
		{
			if (p[i] < p[mini])
			{
				mini = i;
			}
			if (p[i] > p[maxi])
			{
				maxi = i;
			}
		}
		Swap(&p[begin], &p[mini]);
		if (begin == maxi)
		{
			maxi = mini;
		}
		Swap(&p[end], &p[maxi]);
		begin++;
		end--;
	}

}

void AdjustUp(int* p, int child)
{
	int parent = (child - 1) / 2;
	while (child > 0)
	{
		if (p[child] > p[parent])
		{
			Swap(&p[child], &p[parent]);
			child = parent;
			parent = (child - 1) / 2;
		}
		else
		{
			break;
		}
	}
}

void AdjustDowm(int* p, int n, int parent)
{
	int child = parent * 2 + 1;
	while (child < n)
	{
		if (child + 1 < n && p[child + 1] > p[child])
		{
			child++;
		}
		if (p[child] > p[parent])
		{
			Swap(&p[child], &p[parent]);
			parent = child;
			child = parent * 2 + 1;
		}
		else
		{
			break;
		}
	}
}

void HeapSort(int* p, int n)
{
	//建堆
	//for (int i = (n - 1 - 1) / 2; i >= 0; i--)
	//{
	//	AdjustDowm(p, n, i);
	//}

	for (int i = 1; i < n; i++)
	{
		AdjustUp(p, i);
	}
	Print(p, n);

	//调堆
	for (int end = n - 1; end > 0; end--)
	{
		Swap(&p[0], &p[end]);
		AdjustDowm(p, end, 0);
	}
}

int main()
{
	int arr[] = { 4,7,9,0,3,2,8,1,5,6 };
	int sz = sizeof(arr) / sizeof(arr[0]);

	////插入排序
	//InsertSort(arr, sz);

	////希尔排序
	//ShellSort(arr, sz);
	

	////选择排序
	//SelectSort(arr, sz);
	
	//堆排序
	HeapSort(arr, sz);
	Print(arr, sz);
	return 0;
}