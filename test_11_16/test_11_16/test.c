#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>

void Print(int* p, int n)
{
	for (int i = 0; i < n; i++)
	{
		printf("%d ", p[i]);
	}
	printf("\n");
}

void Swap(int* px, int* py)
{
	int tmp = *px;
	*px = *py;
	*py = tmp;
}

int Partion(int* p, int left, int right)
{
	int keyi = left;
	while (left < right)
	{
		while (left < right && p[right] >= p[keyi])
		{
			right--;
		}
		while (left < right && p[left] <= p[keyi])
		{
			left++;
		}
		Swap(&p[left], &p[right]);
	}
	Swap(&p[right], &p[keyi]);
	return right;
}


void QuickSort(int* p, int left,int right)
{
	if (left >= right)
	{
		return;
	}
	int keyi = Partion(p, left, right);
	QuickSort(p, left, keyi - 1);
	QuickSort(p, keyi + 1, right);
}


int main()
{
	int arr[] = { 5,2,6,9,1,3,4,0,8,7 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	QuickSort(arr, 0,sz-1);
	Print(arr, sz);
	return 0;
}