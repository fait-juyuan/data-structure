#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <stdlib.h>
#include "Stack.h"
//int Partion3(int* a, int left, int right)
//{
//	int prev = left;
//	int cur = left+1;
//	int key = a[left];
//
//	while (cur<=right)
//	{
//		if (a[cur] < key)
//		{
//			prev++;
//			Swap(&a[cur], &a[prev]);
//			cur++;
//		}
//		else
//		{
//			cur++;
//		}
//	}
//
//}

void Swap(int* px, int* py)
{
	int tmp = *px;
	*px = *py;
	*py = tmp;
}
void Print(int* p, int n)
{
	for (int i = 0; i < n; i++)
	{
		printf("%d ", p[i]);
	}
	printf("\n");
}

int Partion3(int* p, int left, int right)
{
	int keyi = left;
	int prev = left;
	int cur = left + 1;
	//把小的数往前面翻，把大的数往后面推
	while (cur <= right)
	{
		if (p[cur] < p[keyi])
		{
			prev++;
			Swap(&p[prev], &p[cur]);
		}
		cur++;
	}
	Swap(&p[prev], &p[keyi]);
	return prev;
}

void QuickSort(int* p, int left, int right)
{
	if (left >= right)
	{
		return;
	}
	int keyi = Partion3(p, left, right);
	QuickSort(p, left, keyi - 1);
	QuickSort(p, keyi + 1, right);
}

void _MergeSort(int* p, int left, int right, int* tmp)
{
	if (left >= right)
	{
		return;
	}
	int mid = (left + right) / 2;
	_MergeSort(p, left, mid,tmp);
	_MergeSort(p, mid + 1, right, tmp);

	int begin1 = left, end1 = mid;
	int begin2 = mid + 1, end2 = right;
	int i = left;
	while (begin1 <= end1 && begin2 <= end2)
	{
		if (p[begin1] < p[begin2])
		{
			tmp[i] = p[begin1];
			begin1++;
			i++;
		}
		else
		{
			tmp[i] = p[begin2];
			begin2++;
			i++;
		}
	}

	while (begin1 <= end1)
	{
		tmp[i] = p[begin1];
		begin1++;
		i++;
	}
	while (begin2 <= end2)
	{
		tmp[i] = p[begin2];
		begin2++;
		i++;
	}

	for (int j = left; j <= right; ++j)
	{
		p[j] = tmp[j];
	}
}

void MergeSort(int* p, int n)
{
	int* tmp = (int*)malloc(sizeof(int) * n);
	if (tmp == NULL)
	{
		perror("erron ");
		exit(-1);
	}
	_MergeSort(p, 0, n - 1, tmp);
}


void QuickSortNonR(int* p, int left, int right)
{
	ST st;
	StackInit(&st);
	StackPush(&st, left);
	StackPush(&st,right);

	while (!StackEmpty(&st))
	{
		int right = StackTop(&st);
		StackPop(&st);

		int left = StackTop(&st);
		StackPop(&st);

		int keyi = Partion3(p, left, right);
		if (left < keyi-1)
		{
			StackPush(&st, left);
			StackPush(&st, keyi - 1);
		}
		if (keyi + 1 < right)
		{
			StackPush(&st, keyi + 1);
			StackPush(&st, right);
		}
	}

}

int main()
{
	int arr[] = { 4,2,9,7,1,3,6,5,8,99 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	//QuickSort(arr, 0, sz-1);
	//MergeSort(arr, sz);
	QuickSortNonR(arr, 0, sz - 1);
	Print(arr, sz);
	return 0;
}