#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <stdlib.h>


void Print(int* p, int n)
{
	int i = 0;
	for (i = 0; i < n; i++)
	{
		printf("%d ", p[i]);
	}
	printf("\n");
}


void InsertSort(int* p, int n)
{
	int i = 0;
	//外层循环控制产生有序区间的最后一个元素的下标
	for (i = 0; i < n - 1; i++)
	{
		int end = i; //end为有序区间的最后一个元素的下标
		int x = p[end + 1]; //x为要插入的元素


		//内层循环控制找比x要小的元素，有两种可能
		//1.在数组中可以找到，从break退出
		//2.在数组中找不到比自己小的，从while(end>=0)退出
		while (end >= 0)
		{
			if (p[end] > x)
			{
				p[end + 1] = p[end];
				end--;
			}
			else
			{
				break;
			}
		}
		//最后把保存的x值插入即可
		p[end + 1] = x;
	}
}

void ShellSort(int* p, int n)
{
	int gap = n;
	int i = 0;
	while (gap > 1)
	{
		gap = gap / 2;
		for (int j = 0; j < gap; j++) {
			for (i = j; i < n - gap; i += gap)
			{
				int end = i;
				int x = p[end + gap];
				while (end >= 0)
				{
					if (p[end] > x)
					{
						p[end + gap] = p[end];
						end = end - gap;
					}
					else
					{
						break;
					}
				}
				p[end + gap] = x;
			}
		}
	}

}


int main()
{
	int arr[] = { 6,2,5,3,9,4,8,5,1,7 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	//InsertSort(arr, sz);
	ShellSort(arr, sz);
	Print(arr, sz);
	return 0;
}


