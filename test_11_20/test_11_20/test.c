#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <stdlib.h>
#include "Queue.h"
typedef char BTDataType;

typedef struct BinaryTreeNode
{
	BTDataType data;
	struct BinaryTreeNode* left;
	struct BinaryTreeNode* right;
}BTNode;

//int main()
//{
	//BTNode* a = (BTNode*)malloc(sizeof(BTNode));
	//BTNode* b = (BTNode*)malloc(sizeof(BTNode));
	//BTNode* c = (BTNode*)malloc(sizeof(BTNode));
	//BTNode* d = (BTNode*)malloc(sizeof(BTNode));
	//BTNode* e = (BTNode*)malloc(sizeof(BTNode));
	//BTNode* f = (BTNode*)malloc(sizeof(BTNode));

	//a->left = b;
	//a->right = c;
	//b->left = d;
	//b->right = NULL;
	//d->left = NULL;
	//d->right = NULL;
	//c->left = e;
	//c->right = f;
	//e->left = NULL;
	//e->right = NULL;
	//f->left = NULL;
	//f->right = NULL;
//}

BTNode* BuyNode(BTDataType x)
{
	BTNode* tmp = (BTNode*)malloc(sizeof(BTNode));
	if (tmp == NULL)
	{
		perror("erron ");
		exit(-1);
	}
	tmp->data = x;
	tmp->left = NULL;
	tmp->right = NULL;
	return tmp;

}

BTNode* CreatBinaryTree()
{
	BTNode* a = BuyNode('A');
	BTNode* b = BuyNode('B');
	BTNode* c = BuyNode('C');
	BTNode* d = BuyNode('D');
	BTNode* e = BuyNode('E');
	BTNode* f = BuyNode('F');
	BTNode* g = BuyNode('G');

	a->left = b;
	a->right = c;
	b->left = d;
	c->left = e;
	c->right = f;
	f->left = g;

	return a;
}

void PrevOrder(BTNode* root)
{
	if (root == NULL)
	{
		printf("NULL ");
		return;
	}
	printf("%c ", root->data);
	PrevOrder(root->left);
	PrevOrder(root->right);
}

void InOrder(BTNode* root)
{
	if (root == NULL)
	{
		printf("NULL ");
		return;
	}
	InOrder(root->left);
	printf("%c ", root->data);
	InOrder(root->right);
}

void PostOrder(BTNode* root)
{
	if (root == NULL)
	{
		printf("NULL ");
		return;
	}
	PostOrder(root->left);
	PostOrder(root->right);
	printf("%c ", root->data);
}

void DestroyTree(BTNode* root)
{
	if (root == NULL)
	{
		return;
	}
	DestroyTree(root->left);
	DestroyTree(root->right);
	free(root);
}

int  BinaryTreeSize(BTNode* root)
{
	if (root == NULL)
	{
		return 0 ;
	}
	return BinaryTreeSize(root->left) + BinaryTreeSize(root->right) + 1;
}

int BinaryTreeLeafSize(BTNode* root)
{
	if (root == NULL)
	{
		return 0;
	}
	if (root->left == NULL && root->right == NULL)
	{
		return 1;
	}
	return BinaryTreeLeafSize(root->left) + BinaryTreeLeafSize(root->right);
}


// 二叉树第k层节点个数
int BinaryTreeLevelKSize(BTNode* root, int k)
{
	if (root == NULL)
	{
		return 0;
	}
	if (k == 1)
	{
		return 1;
	}
	return BinaryTreeLevelKSize(root->left, k - 1) 
		+ BinaryTreeLevelKSize(root->right, k - 1);
}

int BinaryTreeDepth(BTNode* root)
{
	if (root == NULL)
	{
		return  0;
	}
	int leftDepth = BinaryTreeDepth(root->left);
	int rightDepth = BinaryTreeDepth(root->right);

	return leftDepth > rightDepth ? leftDepth + 1 : rightDepth + 1;
}

BTNode* BinaryTreeFind(BTNode* root, BTDataType x)
{
	if (root == NULL)
	{
		return NULL;
	}
	if (root->data == x)
	{
		return root;
	}
	BTNode* left = BinaryTreeFind(root->left,x);
	if (left != NULL)
	{
		return left;
	}
     
	BTNode* right = BinaryTreeFind(root->right, x);
	if (right != NULL)
	{
		return right;
	}
	return NULL;
}

void BinaryTreeOrder(BTNode* root)
{
	if (root == NULL)
	{
		return;
	}
	Queue q;
	QueueInit(&q);
	QueuePush(&q, root);

	while (!QueueEmpty(&q))
	{
		BTNode* front = QueueFront(&q);
		printf("%c", front->data);
		QueuePop(&q);

		if (front->left != NULL)
		{
			QueuePush(&q, front->left);
		}
		if (front->right != NULL)
		{
			QueuePush(&q, front->right);
		}
	}
	QueueDestroy(&q);

}

bool BinaryTreeComplete(BTNode* root)
{
	if (root == NULL)
	{
		return;
	}
	Queue q;
	QueueInit(&q);
	QueuePush(&q, root);

	while (!QueueEmpty(&q))
	{
		BTNode* front = QueueFront(&q);
		QueuePop(&q);
		if (front != NULL)
		{
			QueuePush(&q, front->left);
			QueuePush(&q, front->right);
		}
		else
		{
			break;
		}
	}

	while (!QueueEmpty(&q))
	{
		BTNode* front = QueueFront(&q);
		if (front != NULL)
		{
			return false;
		}
		QueuePop(&q);
	}

	return true;
}

int main()
{
	BTNode* root = CreatBinaryTree();

	PrevOrder(root);
	printf("\n");
	InOrder(root);
	printf("\n");
	PostOrder(root);
	printf("\n");

	int ret=BinaryTreeSize(root);
	printf("二叉树结点的个数为%d\n", ret);

	ret = BinaryTreeLeafSize(root);
	printf("二叉树叶子结点的个数为%d\n", ret);

	ret = BinaryTreeLevelKSize(root,1);
	printf("二叉树第k层的结点个数为%d\n",ret);

	ret = BinaryTreeDepth(root);
	printf("%d\n", ret);
	
	BinaryTreeOrder(root);

	//BinaryTreeComplete(root);
	DestroyTree(root);
	root = NULL;
	return 0;
}