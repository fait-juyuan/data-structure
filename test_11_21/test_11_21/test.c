#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <stdlib.h>
#include "Queue.h"


typedef char BTDataType;

typedef struct BinaryTreeNode
{
	BTDataType data;
	struct BinaryTreeNode* left;
	struct BinaryTreeNode* right;
}BTNode;

//int main()
//{
	//BTNode* a = (BTNode*)malloc(sizeof(BTNode));
	//BTNode* b = (BTNode*)malloc(sizeof(BTNode));
	//BTNode* c = (BTNode*)malloc(sizeof(BTNode));
	//BTNode* d = (BTNode*)malloc(sizeof(BTNode));
	//BTNode* e = (BTNode*)malloc(sizeof(BTNode));
	//BTNode* f = (BTNode*)malloc(sizeof(BTNode));

	//a->left = b;
	//a->right = c;
	//b->left = d;
	//b->right = NULL;
	//d->left = NULL;
	//d->right = NULL;
	//c->left = e;
	//c->right = f;
	//e->left = NULL;
	//e->right = NULL;
	//f->left = NULL;
	//f->right = NULL;
//}

BTNode* BuyNode(BTDataType x)
{
	BTNode* tmp = (BTNode*)malloc(sizeof(BTNode));
	if (tmp == NULL)
	{
		perror("erron ");
		exit(-1);
	}
	tmp->data = x;
	tmp->left = NULL;
	tmp->right = NULL;
	return tmp;

}

BTNode* CreatBinaryTree()
{
	BTNode* a = BuyNode('A');
	BTNode* b = BuyNode('B');
	BTNode* c = BuyNode('C');
	BTNode* d = BuyNode('D');
	BTNode* e = BuyNode('E');
	BTNode* f = BuyNode('F');
	BTNode* g = BuyNode('G');

	a->left = b;
	a->right = c;
	b->left = d;
	c->left = e;
	c->right = f;
	//f->left = g;

	return a;
}

//前序遍历
void PrevOrder(BTNode* root)
{
	if (root == NULL){
		printf("NULL ");
		return;
	}
	printf("%c ", root->data);

	PrevOrder(root->left);

	PrevOrder(root->right);
}

void InOrder(BTNode* root)
{
	if (root == NULL){
		printf("NULL ");
		return;
	}
	InOrder(root->left);

	printf("%c ", root->data);

	InOrder(root->right);
}

void PostOrder(BTNode* root)
{
	if (root == NULL){
		printf("NULL ");
		return;
	}
	PostOrder(root->left);

	PostOrder(root->right);

	printf("%c ", root->data);
}

void DestroyTree(BTNode* root)
{
	if (root == NULL)
	{
		return;
	}
	DestroyTree(root->left);
	DestroyTree(root->right);
	free(root);
}

int  BinaryTreeSize(BTNode* root)
{
	if (root == NULL)
	{
		return 0 ;
	}
	//先求左右子树的个树再加上根结点就是总的结点个数
	return BinaryTreeSize(root->left) +
		   BinaryTreeSize(root->right) + 1;
}

int BinaryTreeLeafSize(BTNode* root)
{
	if (root == NULL)
	{
		return 0;
	}
	if (root->left == NULL && root->right == NULL)
	{
		return 1;
	}
	
	return BinaryTreeLeafSize(root->left) +
		   BinaryTreeLeafSize(root->right);
}


// 二叉树第k层节点个数
int BinaryTreeLevelKSize(BTNode* root, int k)
{
	if (root == NULL)
	{
		return 0;
	}
	if (k == 1)
	{
		return 1;
	}

	return BinaryTreeLevelKSize(root->left, k - 1) 
		+ BinaryTreeLevelKSize(root->right, k - 1);
}

int BinaryTreeDepth(BTNode* root)
{
	if (root == NULL)
	{
		return  0;
	}
	//先求出左右子树的高度
	int leftDepth = BinaryTreeDepth(root->left);
	int rightDepth = BinaryTreeDepth(root->right);

	//比较左右子树的高度，大的加1就是树的高度
	return leftDepth > rightDepth ? leftDepth + 1 : rightDepth + 1;
}

BTNode* BinaryTreeFind(BTNode* root, BTDataType x)
{
	if (root == NULL)
	{
		return NULL;
	}
	if (root->data == x)
	{
		return root;
	}
	BTNode* left = BinaryTreeFind(root->left,x);
	if (left != NULL)
	{
		return left;
	}
     
	BTNode* right = BinaryTreeFind(root->right, x);
	if (right != NULL)
	{
		return right;
	}
	return NULL;
}

void BinaryTreeOrder(BTNode* root)
{
	if (root == NULL)
	{
		return;
	}
	Queue q;
	QueueInit(&q);
	//把根入队列
	QueuePush(&q, root);

	while (!QueueEmpty(&q))
	{
		//取队头的元素
		BTNode* front = QueueFront(&q);
		printf("%c", front->data);
		QueuePop(&q);  //出队列

		if (front->left != NULL)
		{
			QueuePush(&q, front->left);
		}
		if (front->right != NULL)
		{
			QueuePush(&q, front->right);
		}
	}
	//销毁队列
	QueueDestroy(&q);

}

bool BinaryTreeComplete(BTNode* root)
{
	if (root == NULL)
	{
		return false;
	}
	Queue q;
	QueueInit(&q);
	QueuePush(&q, root);

	while (!QueueEmpty(&q))
	{
		BTNode* front = QueueFront(&q);
		QueuePop(&q);
		//如果队头元素不为空就入队头元素的左右子树
		//如果队头元素为空就退出
		if (front != NULL)
		{
			QueuePush(&q, front->left);
			QueuePush(&q, front->right);
		}
		else
		{
			break;
		}
	}

	//判断队列剩下的元素是否都为空指针
	//都为空指针就是完全二叉树
	while (!QueueEmpty(&q))
	{
		BTNode* front = QueueFront(&q);
		if (front != NULL)
		{
			QueueDestroy(&q);
			return false;
		}
		QueuePop(&q);
	}

	return true;
}

int main()
{
	BTNode* root = CreatBinaryTree();

	PrevOrder(root);
	printf("\n");
	InOrder(root);
	printf("\n");
	PostOrder(root);
	printf("\n");

	int ret=BinaryTreeSize(root);
	printf("二叉树结点的个数为%d\n", ret);

	ret = BinaryTreeLeafSize(root);
	printf("二叉树叶子结点的个数为%d\n", ret);

	ret = BinaryTreeLevelKSize(root,1);
	printf("二叉树第k层的结点个数为%d\n",ret);

	ret = BinaryTreeDepth(root);
	printf("%d\n", ret);
	
	BinaryTreeOrder(root);

	//BinaryTreeComplete(root);
	DestroyTree(root);
	root = NULL;
	return 0;
}

int main()
{
	//手动连接结点创建一棵树
	BTNode* root = CreatBinaryTree();
	//前序遍历
	PrevOrder(root);
	printf("\n");
}

int main()
{
	//手动连接节点创建一棵树
	BTNode* root = CreatBinaryTree();

	//统计二叉树的结点个数
	int ret = BinaryTreeSize(root);
	printf("二叉树结点的个数为%d\n", ret);
}