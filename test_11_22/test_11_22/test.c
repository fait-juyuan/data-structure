#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#include <stdlib.h>
struct TreeNode
{
    char data;
    struct TreeNode* left;
    struct TreeNode* right;
};

typedef struct TreeNode TreeNode;

TreeNode* CreatTree(char* str, int* pi)
{
    if (str[(*pi)] == '#')
    {
        (*pi)++;
        return NULL;
    }
    TreeNode* root = (TreeNode*)malloc(sizeof(TreeNode));
    if (root == NULL)
    {
        exit(-1);
    }
    root->data = str[(*pi)];
    (*pi)++;
    root->left = CreatTree(str, pi);
    root->right = CreatTree(str, pi);
    return root;
}

void InOrder(TreeNode* root)
{
    if (root == NULL)
    {
        return;
    }
    InOrder(root->left);
    printf("%c ", root->data);
    InOrder(root->right);
}


//求树的高度函数
int TreeDepth(struct TreeNode* root)
{
    if (root == NULL)
    {
        return 0;
    }
    int leftDepth = TreeDepth(root->left);
    int rightDepth = TreeDepth(root->right);
    return leftDepth > rightDepth ? leftDepth + 1 : rightDepth + 1;


}



//类似于后序遍历，先从最后的左右结点开始验证，不能从头开始验证
bool isBalanced(struct TreeNode* root) {
    if (root == NULL)
    {
        return true;
    }


    //求左子树是否平衡
    if (!isBalanced(root->left))
    {
        return false;
    }

    //求右子树是否平衡
    if (!isBalanced(root->right))
    {
        return false;
    }


    int left = TreeDepth(root->left);
    int right = TreeDepth(root->right);
    //左右子树的高度不超过1就平衡，超过1就不平衡
    if (abs(left - right) > 1)
    {
        return false;
    }
    return true;

}

int main()
{
    //char arr[100];
    //int i = 0;
    //while (scanf("%s", arr) != EOF)
    //{
    //    TreeNode* root = CreatTree(arr, &i);
    //    InOrder(root);
    //}


    


    return 0;
}