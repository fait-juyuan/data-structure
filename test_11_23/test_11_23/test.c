#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <stdlib.h>

void Swap(int* px, int* py)
{
	int tmp = *px;
	*px = *py;
	*py = tmp;
}

void Print(int* p, int n)
{
	int i = 0;
	for (i = 0; i < n; i++)
	{
		printf("%d ", p[i]);
	}
	printf("\n");
}

void BubbleSort(int* p, int n)
{
	int i = 0, j = 0;
	//for (i = 0; i < n - 1; i++)
	//{
	//	int falg = 1;
	//	for (j = 0; j < n - 1 - i; j++)
	//	{
	//		if (p[j] > p[j + 1])
	//		{
	//			falg = 0;
	//			Swap(&p[j], &p[j + 1]);
	//		}
	//	}
	//	if (falg == 1)
	//	{
	//		break;
	//	}
	//}


	int end = n - 1;
	while (end > 0)
	{
		for (j = 0; j < end; j++)
		{
			if (p[j] > p[j + 1])
			{

				Swap(&p[j], &p[j + 1]);
			}
		}
		end--;
	}
}

//ǰ��ָ�뷨
int Partion1(int* p, int left,int right)
{
	int prev = left;
	int cur = left + 1;
	int keyi = left;
	while (cur <= right)
	{
		if (p[cur] < p[keyi])
		{
			prev++;
			Swap(&p[cur], &p[prev]);
		}
		cur++;
	}
	Swap(&p[keyi], &p[prev]);
	return prev;
}



void QuickSort(int* p, int left,int right)
{
	if (left >= right)
	{
		return ;
	}
	int keyi = Partion1(p, left,right);
	QuickSort(p, left, keyi - 1);
	QuickSort(p, keyi + 1, right);
}

int main()
{
	int arr[] = { 3,5,9,7,1,2,6,5,8,4 };
	int sz = sizeof(arr) / sizeof(arr[0]);

	//BubbleSort(arr, sz);
	//Print(arr, sz);

	QuickSort(arr, 0,sz-1);
	Print(arr, sz);
	return 0;
}
