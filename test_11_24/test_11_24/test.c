#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <stdlib.h>
#include "Stack.h"

void Swap(int* px, int* py)
{
	int tmp = *px;
	*px = *py;
	*py = tmp;
}

void Print(int* p, int n)
{
	int i = 0;
	for (i = 0; i < n; i++)
	{
		printf("%d ", p[i]);
	}
	printf("\n");
}

void InsertSort(int* p, int n)
{
	int i = 0;
	for (i = 0; i < n - 1; i++)
	{
		int end = i;
		int x = p[end + 1];
		while (end >= 0)
		{
			if (p[end] > x)
			{
				p[end + 1] = p[end];
				end--;
			}
			else
			{
				break;
			}
		}
		p[end + 1] = x;
	}
}

//三数取中法，把三个数的中间值的下标传回去
int GetMidIndex(int* p, int left, int right)
{
	int midi = (left + right) / 2;
	if (p[left] > p[midi])
	{
		if (p[midi] > p[right])
		{
			return midi;
		}
		else if (p[right] > p[left])
		{
			return left;
		}
		else
		{
			return right;
		}
	}
	else  //p[midi]>p[left]
	{
		if (p[left] > p[right])
		{
			return left;
		}
		else if (p[right] > p[midi])
		{
			return midi;
		}
		else
		{
			return right;
		}
	}
}

//最原始的版本
int Partion1(int* p, int left, int right)
{
	//如果选择左边为key，要从右边开始找比keyi位置还要小的，
	//如果选择右边为key，要从左边开始找比keyi还要大的，
	//下面是选择左边为key，先从右找小，再从左找大
	int keyi = left;
	while (left < right)
	{
		while (left<right && p[right] >= p[keyi])
		{
			right--;
		}
		while (left < right && p[left] <= p[keyi])
		{
			left++;
		}
		Swap(&p[left], &p[right]);
	}
	Swap(&p[keyi], &p[right]);
	return right;
}

int Partion2(int* p, int left, int right)
{
	int pivot = left;
	int key = p[pivot];
	while (left < right)
	{
		while (left<right && p[right]>=key)
		{
			right--;
		}
		p[pivot] = p[right];
		pivot = right;
		while (left < right && p[left] <= key)
		{
			left++;
		}
		p[pivot] = p[left];
		pivot = left;
	}
	p[pivot] = key;
	return pivot;
}

//前后指针法
int Partion3(int* p, int left, int right)
{
	//优化1：三数取中法
	int mid = GetMidIndex(p, left, right);
	//把三个数的中间值换到最左边来
	//三数取中 -- 面对有序最坏情况，变成选中位数做key，变成最好情况
	Swap(&p[mid], &p[left]);


	int prev = left;
	int cur = left + 1;
	int keyi = left;
	while (cur <= right)
	{
		if (p[cur] < p[keyi])
		{
			prev++;
			Swap(&p[prev], &p[cur]);
		}
		cur++;
	}
	Swap(&p[keyi], &p[prev]);
	return prev;
}

void QuickSort(int* p, int left, int right)
{
	if (left >= right)
	{
		return;
	}
	//优化2：减少递归的深度
	if (right - left + 1 < 5)
	{
		//InsertSort(p+left, right - left + 1);
		InsertSort(p+left, right - left + 1);
	}
	else
	{
		int keyi = Partion1(p, left, right);
		QuickSort(p, left, keyi - 1);
		QuickSort(p, keyi + 1, right);
	}


}

void QuickSortNonR(int* p, int left, int right)
{
	assert(left <= right);

	ST st;
	StackInit(&st);
	StackPush(&st, left);
	StackPush(&st, right);

	while (!StackEmpty(&st))
	{
		int end = StackTop(&st);
		StackPop(&st);

		int begin = StackTop(&st);
		StackPop(&st);

		int keyi = Partion3(p, begin, end);
		if (begin < keyi - 1)
		{
			StackPush(&st, begin);
			StackPush(&st, keyi - 1);
		}
		if (keyi + 1 < end)
		{
			StackPush(&st, keyi + 1);
			StackPush(&st, end);
		}
	}

}

void _MergeSort(int* p, int left, int right,int* tmp)
{
	if (left >= right)
	{
		return;
	}
	int midi = (left + right) / 2;
	_MergeSort(p, left, midi, tmp);
	_MergeSort(p, midi + 1, right, tmp);

	int begin1 = left, end1 = midi;
	int begin2 = midi + 1, end2 = right;
	int i = left;
	//把两个数组归并到新数组中
	while (begin1 <= end1 && begin2 <= end2)
	{
		if (p[begin1] < p[begin2])
		{
			tmp[i] = p[begin1];
			i++;
			begin1++;
		}
		else
		{
			tmp[i] = p[begin2];
			i++;
			begin2++;
		}
	}

	//把剩下的元素归并到新数组中
	while (begin1 <= end1)
	{
		tmp[i] = p[begin1];
		i++;
		begin1++;
	}
	while (begin2 <= end2)
	{
		tmp[i] = p[begin2];
		i++;
		begin2++;
	}

	//把归并好的数组拷贝回原来的数组
	int j = 0;
	for (j = left; j <= right; j++)
	{
		p[j] = tmp[j];
	}

}

void MergeSort(int* p, int n)
{
	int* tmp = (int*)malloc(sizeof(int) * n);
	if (tmp == NULL)
	{
		exit(-1);
	}

	//相当于后序遍历
	_MergeSort(p, 0, n - 1,tmp);
}

void MergeSortNonR(int* p, int n)
{
	int* tmp = (int*)malloc(sizeof(int) * n);
	if (tmp == NULL)
	{
		exit(-1);
	}
	int gap = 1;
	while (gap < n)
	{
		int i = 0;
		for (i = 0; i < n; i += 2 * gap)
		{
			//[i,i+gap-1]  [i+gap,i+2*gap-1]
			int begin1 = i, end1 = i + gap - 1;
			int begin2 = i + gap, end2 = i + 2 * gap - 1;
			int j = i;
			//控制边界
			if (end1 >= n)
			{
				end1 = n - 1;
			}
			if (begin2 >= n)
			{
				begin2 = n + 1;
			}
			if (end2 >= n)
			{
				end2 = n - 1;
			}
			while (begin1 <= end1 && begin2 <= end2)
			{
				if (p[begin1] < p[begin2])
				{
					tmp[j] = p[begin1];
					j++;
					begin1++;
				}
				else
				{
					tmp[j] = p[begin2];
					j++;
					begin2++;
				}
			}

			while (begin1 <= end1)
			{
				tmp[j] = p[begin1];
				j++;
				begin1++;
			}
			while (begin2 <= end2)
			{
				tmp[j] = p[begin2];
				j++;
				begin2++;
			}
		}
		for (int i = 0; i < n; i++)
		{
			p[i] = tmp[i];
		}
		gap = gap * 2;
	
	}
}

int main()
{
	int arr[] = { 4,6,8,9,2,3,1,5,7 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	//QuickSort(arr, 0, sz - 1);
	//Print(arr,sz);

	//QuickSortNonR(arr, 0, sz - 1);
	//Print(arr, sz);

	//TestOP();

	//MergeSort(arr, sz);
	//Print(arr, sz);

	MergeSortNonR(arr, sz);
	Print(arr, sz);

	return 0;
}