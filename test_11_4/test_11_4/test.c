#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>


//void AdjustDown(int* a, int n, int parent)
//{
//	int leftChild = parent * 2 + 1;
//	int rightChild = parent * 2 + 2;
//	while (leftChild < n )
//	{
//		int min = 0;
//		if (a[leftChild] < a[rightChild] )
//		{
//			min = leftChild;
//		}
//		else 
//		{
//			min = rightChild;
//		}
//		if (a[min] < a[parent])
//		{
//			Swap(&a[parent], &a[min]);
//			parent = min;
//			leftChild = parent * 2 + 1;
//			rightChild = parent * 2 + 2;
//		}
//		else
//		{
//			break;
//		}
//	}
//
//}





//
//C语言中没有栈，我们先实现一个栈
//构建栈如下：
typedef char STDataType;
struct Stack
{
    STDataType* a;
    int top;
    int capacity;
};
typedef struct Stack ST;

//初始化栈
void StackInit(ST* ps);
//销毁栈
void StackDestroy(ST* ps);
//入栈
void StackPush(ST* ps, STDataType x);
//出栈
void StackPop(ST* ps);
//获取栈的元素个数
int StackSize(ST* ps);
//获取栈顶的元素
STDataType StackTop(ST* ps);
//判断栈是否为空
bool StackEmpty(ST* ps);

void StackInit(ST* ps)
{
    ps->a = NULL;
    ps->capacity = 0;
    ps->top = 0;
}

void StackDestroy(ST* ps)
{
    free(ps->a);
    ps->a = NULL;
    ps->capacity = ps->top = 0;
}

void StackPush(ST* ps, STDataType x)
{
    assert(ps);
    if (ps->capacity == ps->top)
    {
        ps->capacity = ps->capacity > 0 ? ps->capacity * 2 : 2;
        STDataType* tmp =
            (STDataType*)realloc(ps->a, ps->capacity * sizeof(STDataType));
        if (tmp == NULL)
        {
            return;
        }
        else
        {
            ps->a = tmp;
        }
    }

    ps->a[ps->top] = x;
    ps->top++;
}

void StackPop(ST* ps)
{
    assert(ps);
    assert(ps->top > 0);
    ps->top--;
}

int StackSize(ST* ps)
{
    assert(ps);
    assert(ps->top > 0);
    return ps->top;
}

STDataType StackTop(ST* ps)
{
    assert(ps);
    assert(ps->top > 0);
    return ps->a[ps->top - 1];
}

bool StackEmpty(ST* ps)
{
    assert(ps);
    return ps->top == 0;
}



bool isValid(char* s) {
    ST st;
    StackInit(&st);
    while (*s != '\0')
    {
        switch (*s)
        {
            //压栈
        case '(':
        case '[':
        case '{':
            StackPush(&st, *s);
            s++;
            break;
            //取栈顶的数据进行匹配
        case ')':
        case ']':
        case '}':
            //出栈，从栈里面取做括号，栈里面的元素不能为空，
            //防止右括号过多,导致取到栈里面没有元素可以取，这也是不匹配
            if (StackEmpty(&st))
            {
                StackDestroy(&st);
                return false;
            }
            //取栈顶的数据和字符比较
            char top = StackTop(&st);
            StackPop(&st);
            if (top == '(' && *s == ')' ||
                top == '[' && *s == ']' ||
                top == '{' && *s == '}'
                )
            {
                s++;
            }
            else
            {
                StackDestroy(&st);
                return false;
            }
        }
    }

    //最后要判断栈是否为空
    //栈是空的说明才完全匹配，如果还要剩余说明没有是左括号多了，不匹配
    if (StackEmpty(&st))
    {
        StackDestroy(&st);
        return true;
    }
    else
    {
        StackDestroy(&st);
        return false;
    }

}



//C语言中没有队列，我们先构建一个队列的各种接口
//队列实现如下：
typedef int DataType;
typedef struct Node
{
    struct Node* next;
    DataType data;
}Node;

typedef struct Queue
{
    Node* phead;
    Node* tail;
}Queue;

//初始化队列
void QueueInit(Queue* pq);
//销毁队列
void QueueDestroy(Queue* pq);
//队列插数据
void QueuePush(Queue* pq, DataType x);
//队列出数据
void QueuePop(Queue* pq);
//取队尾数据
DataType QueueBack(Queue* pq);
//取队头数据
DataType QueueFront(Queue* pq);
//取队列中元素个数
int QueueSize(Queue* pq);
//判断队列是否为空
bool QueueEmpty(Queue* pq);

void QueueInit(Queue* pq)
{
    assert(pq);
    pq->phead = NULL;
    pq->tail = NULL;
}

void QueuePush(Queue* pq, DataType x)
{
    Node* tmp = (Node*)malloc(sizeof(Node));
    if (tmp == NULL)
    {
        perror("malloc ");
        exit(-1);
    }
    tmp->data = x;
    tmp->next = NULL;
    if (pq->tail == NULL)
    {
        pq->phead = tmp;
        pq->tail = tmp;
    }
    else
    {
        pq->tail->next = tmp;
        pq->tail = tmp;
    }
}

void QueuePop(Queue* pq)
{
    assert(pq);
    assert(pq->phead);
    // 一个节点
    if (pq->phead->next == NULL)
    {
        free(pq->phead);
        pq->phead = NULL;
        pq->tail = NULL;
    }
    //多个节点
    else
    {
        Node* tmp = pq->phead->next;
        free(pq->phead);
        pq->phead = tmp;
    }
}

int QueueSize(Queue* pq)
{
    assert(pq);
    int count = 0;
    Node* cur = pq->phead;
    while (cur != NULL)
    {
        cur = cur->next;
        count++;
    }
    return count;
}

bool QueueEmpty(Queue* pq)
{
    assert(pq);
    return pq->phead == NULL;
}

DataType QueueFront(Queue* pq)
{
    assert(pq);
    assert(pq->phead);
    return pq->phead->data;
}

DataType QueueBack(Queue* pq)
{
    assert(pq);
    assert(pq->phead);
    return pq->tail->data;
}

void QueueDestroy(Queue* pq)
{
    assert(pq);
    Node* tmp = pq->phead;
    while (tmp != NULL)
    {
        Node* next = tmp->next;
        free(tmp);
        tmp = next;
    }
    pq->phead = pq->tail = NULL;
}
//上面都是在实现队列的各种接口

//下面是利用两个队列来实现栈
//用两给队列来实现栈，两个队列不断倒元素即可
typedef struct {
    Queue q1;
    Queue q2;
} MyStack;


MyStack* myStackCreate() {
    MyStack* tmp = (MyStack*)malloc(sizeof(MyStack));
    if (tmp == NULL)
    {
        perror("erron ");
        exit(-1);
    }
    QueueInit(&tmp->q1);
    QueueInit(&tmp->q2);
    return tmp;
}

//往不为空的队列插数据，如果两个队列都为空，任意一个都行
void myStackPush(MyStack* obj, int x) {
    Queue* empty = &obj->q1;
    Queue* nonempty = &obj->q2;
    if (QueueEmpty(&obj->q2))
    {
        Queue* empty = &obj->q2;
        Queue* nonempty = &obj->q1;
    }
    QueuePush(nonempty, x);


}

//先把不为空的队列往空队列里面倒数据，倒到只剩下1个元素
//而这不为空的队列最后一个元素就是栈顶的元素，出掉即可
int myStackPop(MyStack* obj) {
    Queue* empty = &obj->q1;
    Queue* nonempty = &obj->q2;
    if (QueueEmpty(&obj->q2))
    {
        empty = &obj->q2;
        nonempty = &obj->q1;
    }
    while (QueueSize(nonempty) > 1)
    {
        QueuePush(empty, QueueFront(nonempty));
        QueuePop(nonempty);
    }
    //最后一个元素就是栈顶的元素
    //出掉即可,保证队列有一个为空队列
    int tmp = QueueBack(nonempty);
    QueuePop(nonempty);
    return tmp;

}

//直接取不为空的队列的尾元素即可
int myStackTop(MyStack* obj) {
    Queue* empty = &obj->q1;
    Queue* nonempty = &obj->q2;
    if (QueueEmpty(&obj->q2))
    {
        empty = &obj->q2;
        nonempty = &obj->q1;
    }
    return QueueBack(nonempty);
}

bool myStackEmpty(MyStack* obj) {
    //两给队列都为空，栈才是空
    return QueueEmpty(&obj->q1) && QueueEmpty(&obj->q2);
}

void myStackFree(MyStack* obj) {
    QueueDestroy(&obj->q1);
    QueueDestroy(&obj->q2);
    free(obj);
}


//先创建一个结点类型
typedef struct Node
{
    struct Node* next;
    int data;
}Node;


//创建一个循环队列类型结构体
//里面的头指针指向链表的头，尾指针指向链表的尾
typedef struct {
    Node* front;
    Node* tail;
} MyCircularQueue;


//创建循环队列
MyCircularQueue* myCircularQueueCreate(int k) {
    MyCircularQueue* tmp = (MyCircularQueue*)malloc(sizeof(MyCircularQueue));

    //构造一个环链表
    //要多构造一个结点，不然判断不了是否满了
    Node* head = (Node*)malloc(sizeof(Node));
    Node* a = head;
    Node* tail = head;
    while (k--)
    {
        a = tail;
        tail = (Node*)malloc(sizeof(Node));
        a->next = tail;
    }
    tail->next = head;

    tmp->front = head;
    tmp->tail = head;
    return tmp;

}

//向循环队列插入一个元素
bool myCircularQueueEnQueue(MyCircularQueue* obj, int value) {
    if (obj->tail->next == obj->front)
    {
        return false;
    }
    obj->tail->data = value;
    obj->tail = obj->tail->next;
    return true;

}

//从循环队列中删除一个元素
bool myCircularQueueDeQueue(MyCircularQueue* obj) {
    if (obj->front == obj->tail)
    {
        return false;
    }
    obj->front = obj->front->next;
    return true;

}

//从队首获取元素
int myCircularQueueFront(MyCircularQueue* obj) {
    if (obj->front == obj->tail)
    {
        return -1;
    }
    return obj->front->data;

}

//获取队尾元素
int myCircularQueueRear(MyCircularQueue* obj) {
    if (obj->front == obj->tail)
    {
        return -1;
    }
    //找队尾的前一个结点就是队尾数据
    Node* cur = obj->front;
    while (cur->next != obj->tail)
    {
        cur = cur->next;
    }
    return cur->data;

}

//检查循环队列是否为空
bool myCircularQueueIsEmpty(MyCircularQueue* obj) {
    return obj->front == obj->tail;

}

//检查循环队列是否已满。
bool myCircularQueueIsFull(MyCircularQueue* obj) {
    return obj->tail->next == obj->front;

}

//释放内存
void myCircularQueueFree(MyCircularQueue* obj) {
    Node* cur = obj->front;
    while (cur != obj->tail)
    {
        Node* next = cur->next;
        free(cur);
        cur = next;
    }
    free(cur);
    free(obj);
}
