#define _CRT_SECURE_NO_WARNINGS 1
#include "Heap.h"


void HeapInit(Heap* hp)
{
	assert(hp);
	hp->a = NULL;
	hp->capacity = 0;
	hp->size = 0;
}

void HeapDestroy(Heap* hp)
{
	assert(hp);
	free(hp->a);
	hp->capacity = 0;
	hp->size = 0;
}

void Swap(HPDataType* x, HPDataType* y)
{
	HPDataType tmp = *x;
	*x = *y;
	*y = tmp;
}

void AdjustUp(HPDataType* a, int pos)
{
	int child = pos;
	int parent = (child - 1) / 2;
	while (child > 0)
	{
		//建大堆就用大于号，小堆就用小于号，这样好区别是大堆还是小堆
		//当孩子大于父亲就交换
		if (a[child] > a[parent])
		{
			Swap(&a[child], &a[parent]);
			child = parent;
			parent = (child - 1) / 2;
		}
		else
		{
			break;
		}
	}

}



void HeapPush(Heap* hp, HPDataType x)
{
	assert(hp);
	//先在数组里加一个数据
	if (hp->size == hp->capacity)
	{
		hp->capacity = hp->capacity > 0 ? hp->capacity * 2 : 2;
		HPDataType* tmp = (HPDataType*)realloc(hp->a, sizeof(HPDataType) * hp->capacity);
		if (tmp == NULL)
		{
			perror("erron ");
			exit(-1);
		}
		hp->a = tmp;
	}
	hp->a[hp->size] = x;
	hp->size++;

	//向上调整算法
	AdjustUp(hp->a, hp->size - 1);

}

void HeapPrint(Heap* hp)
{
	int i = 0;
	for (i = 0; i < hp->size; i++)
	{
		printf("%d ", hp->a[i]);
	}
	printf("\n");
}


void AdjustDowm(HPDataType* a, int sz,int parent)
{
	//不管建大堆还是小堆，都要先找出两个孩子的最大或者最小值
	//默认孩子是左孩子
	int child = (parent * 2) + 1;
	while (child < sz)
	{
		//建大堆，如果右孩子大于左孩子1，就child++就把左孩子变成右孩子了
		if (child + 1 < sz && a[child + 1] > a[child])
		{
			child++;
		}
		//如果孩子大于父亲就交换
		if (a[child] > a[parent])
		{
			Swap(&a[child], &a[parent]);
			parent = child;
			child = (parent * 2) + 1;
		}
		else
		{
			break;
		}

	}
}

void HeapPop(Heap* hp)
{
	assert(hp);
	assert(hp->size > 0);
	//先把第一个元素和最后一个元素进行交换
	Swap(&hp->a[0], &hp->a[hp->size - 1]);
	hp->size--;

	//调整堆,时间复杂度为 树的高度次
	AdjustDowm(hp->a, hp->size, 0);

}

HPDataType HeapTop(Heap* hp)
{
	assert(hp);
	assert(hp->size > 0);
	return hp->a[hp->size - 1];
}

int HeapSize(Heap* hp)
{
	assert(hp);
	return hp->size;
}

bool HeapEmpty(Heap* hp)
{
	assert(hp);
	return hp->size == 0;
}