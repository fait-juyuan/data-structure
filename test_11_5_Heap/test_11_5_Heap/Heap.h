#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>

typedef int HPDataType;
struct Heap
{
	HPDataType* a;
	int size;
	int capacity;
};
typedef struct Heap Heap;

//�ѵĳ�ʼ��
void HeapInit(Heap* hp);
//�ѵ�����
void HeapDestroy(Heap* hp);
//�ѵĲ���
void HeapPush(Heap* hp,HPDataType x);
//�ѵ�ɾ��
void HeapPop(Heap* hp);
//��ӡ��
void HeapPrint(Heap* hp);
//��ȡ�Ѷ���Ԫ��
HPDataType HeapTop(Heap* hp);
//�ж϶��Ƿ�Ϊ��
bool HeapEmpty(Heap* hp);


void AdjustUp(HPDataType* a, int pos);