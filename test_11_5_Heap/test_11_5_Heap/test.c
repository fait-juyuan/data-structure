#define _CRT_SECURE_NO_WARNINGS 1
#include "Heap.h"

int main()
{
	int arr[] = { 1,5,3,8,7,6 ,9,2 };
	Heap hp;
	HeapInit(&hp);

	int sz = sizeof(arr) / sizeof(arr[0]);
	printf("建大堆,插入元素：\n");
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		HeapPush(&hp, arr[i]);
	}
	HeapPrint(&hp);

	printf("删除元素：\n");
	for (i = 0; i < sz; i++)
	{
		HeapPop(&hp);
		HeapPrint(&hp);
	}
	return 0;
}