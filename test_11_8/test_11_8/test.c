#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <assert.h>


void Print(int* a,int n)
{
	int i = 0;
	for (i = 0; i < n; i++)
	{
		printf("%d ", a[i]);
	}
	printf("\n");
}

void Swap(int* px, int* py)
{
	int tmp = *px;
	*px = *py;
	*py = tmp;
}

//自下而上方式插入建堆
void AdjustUp(int* a, int child)
{
	int parent = (child - 1) / 2;
	while (child > 0)
	{
		//孩子大于父亲就交换，建的是大堆
		//孩子小于父亲就交换，建的是小堆，下面建的是小堆
		if (a[child] < a[parent])
		{
			Swap(&a[child], &a[parent]);
			child = parent;
			parent = (child - 1) / 2;
		}
		else
		{
			break;
		}
	}
}

void AdjustDowm(int* a, int n, int parent)
{
	//默认孩子是左孩子
	int child = (parent * 2) + 1;
	while (child < n)
	{
		//先找出两个孩子最大的那个或者最小的那个
		//这里建的是小堆，找出小的那个
		if (child + 1 < n && a[child + 1] < a[child])
		{
			child++;
		}
		//如果孩子小于父亲就交换
		if (a[child] < a[parent])
		{
			Swap(&a[child], &a[parent]);
			parent = child;
			child = parent * 2 + 1;
		}
		else
		{
			break;
		}
	}
}

//堆排序
void HeapSort(int* a,int n)
{
	int i = 0;
	//建堆有两种方式  
	//1.自下而上方式插入建堆
	//2.自上而下方式分治思想建堆，从最小的堆开始调整
	
	////方式1建堆
	//for (i = 1; i < n; i++)
	//{
	//	AdjustUp(a, i);
	//}
	//printf("建堆的结果为：\n");
	//Print(a, n);


	//方式2建堆
	for (i = (n - 1 - 1) / 2; i >= 0; i--)
	{
		AdjustDowm(a, n, i);
	}
	printf("建堆的结果为：\n");
	Print(a, n);




	//堆排序，自上而下
	int end = 0;
	for (end = n - 1; end > 0; end--)
	{
		//最大或者最小的数交换到后面去
		Swap(&a[0], &a[end]);
		AdjustDowm(a, end, 0);
	}
	printf("堆排序的结果为：\n");
	Print(a, n);

}


int main()
{

	int arr[] = { 4,8,2,7,1,9,3,6,5 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	HeapSort(arr, sz);
	return 0;
}
